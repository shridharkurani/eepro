<script type="text/javascript">var base_url = "<?php echo base_url();?>";</script>
<footer>
<div id="footer" class="container-fluid">
    <div class="row-fluid footer-rows ">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="container">
                <div class="row">
                    <div class="row text-center muted">
                        <div class="footer-social">
                        <br>
                        <div class="icons">
                                        <div class="pull-right">
                                          No 1213, 1st Floor, 22nd Cross, 3rd Sector, HSR Layout, Bangalore 
                                          <br>
                                          Email : info@eepro.com
                                        </div>
                            <a target="_blank" href="#"><i class="fa fa-twitter fa-2x"></i></a>
                            <a target="_blank" href="https://www.facebook.com/themediabasket/"><i class="fa fa-facebook fa-2x"></i></a>
                            <a target="_blank" href="https://www.linkedin.com/company-beta/13211933/"><i class="fa fa-linkedin fa-2x"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-google-plus fa-2x"></i></a>
                        </div>
        
                        </div>
                    </div>
                    </div>
                        <div class="row">
                          <center><small>© 2020 EEPRO. All rights reserved.</small></center>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!--<script type="text/javascript">
        function downloadPage() {
            var pdf = new jsPDF('*');
            pdf.fromHTML($('#printPage').get(0), 10, 10, {
                'width': 180
            });

            pdf.canvas.height = 72 * 11;
            pdf.canvas.width = 72 * 8.5;

            // pdf.fromHTML(window.body);

            pdf.save('test.pdf');
        }
    </script>-->
</footer>
</html>