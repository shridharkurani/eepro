    <body>
    
    <nav class="navbar navbar-default" >
       <div class="container-fluid"  style="background: #172434;">
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <div class="logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url()."logo/logo.png";?>" alt="<?php echo SITE_NAME;?>" title="<?php echo SITE_NAME;?>" style="width: auto ; height:100px;" class="img-responsive"></a></div>
         </div>
         <div id="navbar" class="navbar-collapse collapse">
           <ul class="nav navbar-nav navbar-right"  style="margin-top:2em; padding:0.5em;">
           <!--<li class="menu-item">
               <a href="<?php /*echo base_url().'#whatsmediabasket'; */?>">About us</a>
           </li>-->
           <?php
             if($this->session->userdata('logged_in'))
             {
               echo '<li class="dropdown menu-item-has-children" style="background:#172434 !important;">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" style="background: #172434 !important;" aria-expanded="false">'.strtoupper($this->session->userdata('user_name')).' <span class="caret"></span></a>
               <ul class="dropdown-menu" >
               <li><a href='.base_url().'company/>Company Profile</a></li>
               <li><a href='.base_url().'products/>Products</a></li>
               <li><a href='.base_url().'customers/listing>Customers</a></li>
               <li><a href='.base_url().'account/logout>Logout</a></li> 
               </ul>
               </li>'
               ;
             }
             else{
               echo '<a href='.base_url().'account/signin class="button login-open" style="margin:0 24px !important;">Log In</a>';
               echo '<a href='.base_url().'account/signup class="button border" style="margin: 0 24px !important ;">Sign Up</a>';
             }
             ?>
           </ul>
         </div><!--/.nav-collapse -->
       </div><!--/.container-fluid -->
     </nav>

