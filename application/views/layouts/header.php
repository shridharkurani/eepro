<!DOCTYPE html>
<?php 
$this->benchmark->mark('code_start');
?>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>

  <meta content="utf-8" http-equiv="encoding">
  <title><?php echo $layout_title.' - '.$layout_description; ?></title>
  <?php echo $this->layouts->print_includes(); ?>

  <meta name="description" content="<?php echo DEFAULT_SITE_DESC;?>" />

  <meta name="keywords" content="<?php echo SITE_KEYWORDS; ?>"/>


<!-- Favicons ================================================== -->
<link rel="shortcut icon" href="<?php echo base_url()."assets/local_image/icos/favicon.png"; ?>" type="image/x-icon">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()."assets/local_image/icos/favicon72.ico"; ?>" >
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()."assets/local_image/icos/favicon114.ico"; ?>" >
<!-- Favicons Ends here ======================================== -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
</head>
<body>