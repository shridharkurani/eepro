<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//echo form_open_multipart(current_url(), array("id" => 'previewPage'));
if($this->session->flashdata('msg')){
    echo '<div class="alert alert-success row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';
}
if(isset($err_msg)){ 
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';
}
if (isset($msg)) {
    echo '<div class="alert alert-success row"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';                                 
}
?>
<div class="container" id="printPage">
<?php
if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
/*echo '<ul class="breadcrumb row custom-breadcrumb">';
 $count = count($breadcrumb_array);
 foreach ($breadcrumb_array as $key => $value) {
   if (--$count <= 0) {
     echo "<li class='active'>".$key."</li>";
       break;
   }
   echo '<li><a href='.$value.'>'.$key.'</a></li>';
 }
echo "</ul>";*/
}
//Setting all the required data here.
$cmpnyLogo = $companyAndUserDetails['cmpny_logo_cds'];
$cmpnyAddress = $companyAndUserDetails['quotation_validity_cds'];
?>
<link href="<?php echo asset_url().'css/print.css';?>" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
    <div class="well">
        <legend>
            <div class="col-md-12 text-right btns">
                <!--<input type="submit" name="cancel" value="Cancel" onclick="window.print()" class="btn btn-danger">-->
                <!--<input id="confirm" type="submit" name="save" value="Confirm this quotation" onclick="alert('Thank you for confirming the quotatoin.');$(this).hide();" class="btn btn-success" id-cmpny="<?php /*echo $companyId;*/?>">-->
                <input id="printDownload" type="submit" name="save" value="Print / Download " onclick="window.print()" class="btn hoverable_btn">
<!--                <input id="btnPrint" type="submit" name="saveAndNext" onclick="downloadPage()" value="Download" class="btn hoverable_btn">-->
            </div>
        </legend>
    <section class="printsection">
        <div class="fromAndToDetails">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                    <thead>
                        <tr>
                            <th class="text-left"><img width="35%" src="<?php echo asset_url()."uploads/".$cmpnyLogo; ?>" class="img-responsive img-thumbnail" width="120px;"/></th>
                            <th class="text-right"><?php echo $cmpnyAddress;?></th>
                        </tr>
                        <tr>
                            <th class="text-left">
                                <?php echo "GST Number : " . strtoupper($companyAndUserDetails['cmpnyfax_cds']);?>
                            </th>
                           <th class="text-right">
                               <?php echo $quotationOrInvoiceString ." Number : " . strtoupper($extraDetails['quotationNumber']);?>
                               <br> Date : <?php echo date('d/m/Y');?></th>
                        </tr>
                    </thead>
                    </table>

                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="thead-dark">
                            <th colspan="4" scope="col" class="text-center"><?php echo $quotationOrInvoiceString . " To"; ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row" colspan="2">Company / Customer Name</th>
                            <td><?php echo $customerdetails['company_or_customer_name_cus'];?></td>

                        </tr>
                        <tr>
                            <th scope="row" colspan="2">Contact Person Name</th>
                            <td><?php echo $customerdetails['fullname_cus'];?></td>
                        </tr>
                        <tr>
                            <th scope="row" colspan="2">Mobile Number</th>
                            <td><?php echo $customerdetails['mobile_cus'];?></td>
                        </tr>
                        <tr>
                            <th scope="row" colspan="2">Email</th>
                            <td><?php echo $customerdetails['email_cus'];?></td>
                        </tr>
                        <?php
                        if(!empty($customerdetails['fax_cus'])) {    ?>
                            <tr>
                                <th scope="row" colspan="2">GST Number</th>
                                <td><?php echo $customerdetails['fax_cus']; ?></td>
                            </tr>
                            <?php
                        }

                        ?>
                        <tr>
                            <th scope="row" colspan="2">Shipping Address</th>
                            <td><?php echo strip_tags($customerdetails['shipping_address_cus']);?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div  class="col-md-12">
                    <table class="table table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th colspan="6" scope="col" class="text-center thead-dark">Product Details</th>
                        </tr>
                        <tr>
                            <th>Product Pic</th>
                            <th>Product Description</th>
                            <th>Details</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($productDetails as $product) {                                echo "<tr>";
                                echo '<td><img src="'.$product["productImg"] . '" class="img-responsive img-thumbnail" width="120px;"></td>';
                                echo '<td>'.$product["productName"].'</td>';
                                echo '<td>'.$qtyArray[$product["productId"]]['productDesc'].'</td>';
                                echo '<td>'.$qtyArray[$product["productId"]]['unitprice'].'</td>';
                                echo '<td>'.$qtyArray[$product["productId"]]['qty'].'</td>';
                                echo '<td>'.$qtyArray[$product["productId"]]['price'].'</td>';
                                echo "</tr>";
                               }
                        ?>
                        <tr><td colspan="2"></td><th colspan="2"  class="text-right">Amount : </th> <th colspan="2"  class="text-right"><?php echo $netArray['netTotal'];?></th></tr>
                        <tr><td colspan="2"></td><th colspan="2"  class="text-right">GST @ <?php echo $extraDetails['gstCharges'];?>% : </th> <th  class="text-right" colspan="2"><?php echo $netArray['gst'];?></th></tr>
<!--                        <tr><td colspan="2"></td><th colspan="2">Sub Total : </th> <th>--><?php //echo $netArray['grandTotal'];?><!--</th></tr>-->
                        <tr><td colspan="2"></td><th colspan="2"  class="text-right">Shipping Charges : </th> <th  class="text-right" colspan="2"><?php echo $netArray['shippingCharges'];?></th></tr>
                        <tr><td colspan="2"></td><th colspan="2"  class="text-right">Total : </th> <th colspan="2"  class="text-right"><?php echo $netArray['grandTotal'];?></th></tr>

                        <tr></tr>
                        </tbody>
                    </table>
                </div>

                    <div  class="col-md-12">
                        <table class="table">
                            <tbody>
                            <tr class="thead-dark">
                                <th>Terms and conditions</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $companyAndUserDetails['price_terms_cds'];?>
                                </td>
                            </tr>
                            <tr class="thead-dark">
                                <th>Payment Terms</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $companyAndUserDetails['payment_terms_cds'];?>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div  class="col-md-12 row"><small><?php echo "<p>NOTE : </p>" . $companyAndUserDetails['note_cds'];?></small></div>
                    </div>
                    <legend>&nbsp</legend>
                    <div  class="col-md-12">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td><?php echo strtoupper($companyAndUserDetails['cmpnyname_cds']);?></td>
                                <td class="text-right"><?php echo "Contact Email :". $companyAndUserDetails['cmpnyemail_cds'];?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </section>
    </div>
</div>

