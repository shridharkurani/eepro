<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

echo form_open_multipart(current_url());

if($this->session->flashdata('msg')){
    echo '<div class="alert alert-success row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';
}
if(isset($err_msg)){ 
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';
}
if (isset($msg)) {
    echo '<div class="alert alert-success row"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';                                 
}

if(empty($posted_data) && !empty($cmpnyDetails)) {
    //Default set values from db.
    $cmpnyFullName = $cmpnyDetails['cmpnyname_cds'];
    $cmpnyImage = $cmpnyDetails['cmpny_logo_cds'];
    $contactPersonFullName = $cmpnyDetails['cmpny_contact_person_cds'];
    $cmpnyEmail = $cmpnyDetails['cmpnyemail_cds'];
    $telNumber = $cmpnyDetails['cmpnymobile_cds'];
    $faxNumber = $cmpnyDetails['cmpnyfax_cds'];

    $priceTerms = $cmpnyDetails['price_terms_cds'];
    $paymentTerms = $cmpnyDetails['payment_terms_cds'];
    $quotationValidity = $cmpnyDetails['quotation_validity_cds'];
    $note = $cmpnyDetails['note_cds'];
}
else if ($posted_data) {
    $cmpnyFullName = $posted_data['cmpnyFullName'];
    $cmpnyImage = isset($posted_data['userfile']) ? $posted_data['userfile'] : '' ;
    $contactPersonFullName = $posted_data['contactPersonFullName'];
    $cmpnyEmail = $posted_data['cmpnyEmail'];
    $telNumber = $posted_data['telNumber'];
    $faxNumber = $posted_data['faxNumber'];
    $priceTerms = $posted_data['priceTerms'];
    $paymentTerms = $posted_data['paymentTerms'];
    $quotationValidity = $posted_data['quotationValidity'];
    $note = $posted_data['note'];
}
else {
    $cmpnyFullName = '';
    $cmpnyImage = '';
    $contactPersonFullName = '';
    $cmpnyEmail = '';
    $telNumber = '';
    $faxNumber = '';
    $priceTerms = '';
    $paymentTerms = '';
    $quotationValidity = '';
    $note = '';
}
?>
<div class="container">
<?php
if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
echo '<ul class="breadcrumb row custom-breadcrumb">';
 $count = count($breadcrumb_array);
 foreach ($breadcrumb_array as $key => $value) {
   if (--$count <= 0) {
     echo "<li class='active'>".$key."</li>";
       break;
   }
   echo '<li><a href='.$value.'>'.$key.'</a></li>';
 }
echo "</ul>";
}
?>
    <div class="row well">
       <legend><label class="default_font_color">Company Profile</label>
            <input type="submit" name="" value="Submit" class="btn hoverable_btn pull-right" /></legend>

        <legend>&nbsp</legend>
        <div class="col-md-12">
            <div class="col-md-6">
                <label>Company Full Name <i class="required"> * </i></label>
                <input name="cmpnyFullName" class="form-control" type="text" value="<?php echo set_value('cmpnyFullName',$cmpnyFullName); ?>">

            </div>
            <div class="col-md-6">
                <label>Company Logo<i class="required"> * </i></label>
                <small class="col-md-12"> (Image max size:2MB & Image type: gif, jpg, png.)</small>
                <input name="userfile" class="form-control" type="file" />
                <label>&nbsp </label>
                <img src="<?php echo asset_url()."uploads/".$cmpnyImage; ?>" class="img-responsive img-thumbnail" width="100px;"/>
            </div>
        </div>
        <legend></legend>
        <div class="col-md-12">
            <div class="col-md-3">
                <label>Contact Person Full Name<i class="required"> * </i></label>
                <input name="contactPersonFullName" class="form-control" type="text" type="text" value="<?php echo set_value('contactPersonFullName',$contactPersonFullName); ?>">
            </div>
            <div class="col-md-3">
               <label>Email<i class="required"> * </i></label>
               <input id="locality" name="cmpnyEmail" class="form-control" value="<?php echo set_value('cmpnyEmail',$cmpnyEmail); ?>" >
            </div>

            <div class="col-md-3">
               <label>Telephone Number <i class="required"> * </i></label>
               <input name="telNumber" class="form-control" type="text" value="<?php echo set_value('telNumber',$telNumber); ?>">
            </div>
            <div class="col-md-3">
                <label>GST Number</label>
                <input name="faxNumber" class="form-control" type="text" value="<?php echo set_value('faxNumber',$faxNumber); ?>">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <label>Company Address<i class="required"> * </i></label>
                <textarea name="quotationValidity" class="form-control" ><?php echo $quotationValidity;?></textarea>
            </div>
        </div>

        <div class="col-md-12">
            <legend>&nbsp</legend>
            <legend><label class="default_font_color">Sales terms and conditions</label></legend>
            <div class="col-md-12">
                <label>Terms and conditions <i class="required"> * </i></label>
                <textarea name="priceTerms" class="form-control" > <?php echo $priceTerms;?></textarea>
            </div>
            <div class="col-md-12">
                <label>Payment Terms <i class="required"> * </i></label>
                <textarea name="paymentTerms" class="form-control" ><?php echo $paymentTerms;?></textarea>
            </div>
            <div class="col-md-12">
                <label>Note <i class="required"> * </i></label>
                <textarea name="note" class="form-control" ><?php echo $note;?></textarea>
            </div>
        </div>
        <div class="col-md-12 text-center">
            <label class=""><input type="submit" name="" value="Submit" class="btn hoverable_btn"></label>
        </div>
    </div>
</div>
