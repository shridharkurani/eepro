<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

echo form_open_multipart(current_url());

if($this->session->flashdata('msg')){
    echo '<div class="alert alert-success row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';
}
if(isset($err_msg)){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';
}
if (isset($msg)) {
    echo '<div class="alert alert-success row"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

if (validation_errors()){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}
?>

    <div class="page-canvas">
        <div class="signin-wrapper jumbotron" data-login-message="false">
        <div class="row">
            <h3 class="text-center">Sign in to <?php echo SITE_NAME;?></h3>
        </div>
            <section class="tab-pane fade in active" id="signin">
                <?php echo form_open('account/signin','id=account_sigin_frm'); ?>

                <label>Email / Mobile </label><i class="redstar">*</i>
                <input class="form-control" type="text" name="userEmail" value="<?php echo set_value('userEmail'); ?>" >

                <label>Password</label><i class="redstar">*</i>
                <input class="form-control" type="password" name="password" value="" >

                <div class="row text-center">
                    <label><button type="submit" class="submit btn hoverable_btn" name="submit_btn" value="owner">Sign in</button></label>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center text-warning">
                        <small>Please contact us at <?php echo ADMIN_EMAIL; ?> for creation of account or to resolve any problems with the login.</small>
                        <!--<label class="text-right">Don't have a account yet?
                            <a class="forgot pull-center" id="login-signup-link" href="<?php /*echo base_url().'account/signup'; */?>">Signup now »</a>
                        </label>-->
                    </div>
                </div>
                </form>
            </section>

        <input type="hidden" class="verify" name="verify_both"/>

        </div>
    </div>
    </div>
</form>