<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

echo form_open_multipart(base_url().'customers/adding/'.$custmerId);

if($this->session->flashdata('msg')){
    echo '<div class="alert alert-success row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';
}
if(isset($err_msg)){ 
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';
}
if (isset($msg)) {
    echo '<div class="alert alert-success row"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';                                 
}

if(empty($posted_data) && !empty($customerDetails)) {
//    echo "<pre>";print_r($customerDetails);exit();
    //Default set values from db.
    $customerOrCmpnyName = $customerDetails['company_or_customer_name_cus'];
    $contactPersonName = $customerDetails['fullname_cus'];
    $cmpnyEmail = $customerDetails['email_cus'];
    $telNumber = $customerDetails['mobile_cus'];
    $faxNumber = $customerDetails['fax_cus'];
    $customerCurrency = $customerDetails['customer_currency_cus'];
    $shippingAddress = $customerDetails['shipping_address_cus'];
}
else if ($posted_data) {
    $customerOrCmpnyName = $posted_data['customerOrCmpnyName'];
    $contactPersonName = $posted_data['contactPersonName'];
    $cmpnyEmail = $posted_data['cmpnyEmail'];
    $telNumber = $posted_data['telNumber'];
    $faxNumber = $posted_data['faxNumber'];
    $customerCurrency = $posted_data['customerCurrency'];
    $shippingAddress = $posted_data['shippingAddress'];
}
else {
    $customerOrCmpnyName = '';
    $contactPersonName = '';
    $cmpnyEmail = '';
    $telNumber = '';
    $faxNumber = '';
    $customerCurrency = '';
    $shippingAddress = '';
}
?>
<div class="container">
<?php
if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
echo '<ul class="breadcrumb row custom-breadcrumb">';
 $count = count($breadcrumb_array);
 foreach ($breadcrumb_array as $key => $value) {
   if (--$count <= 0) {
     echo "<li class='active'>".$key."</li>";
       break;
   }
   echo '<li><a href='.$value.'>'.$key.'</a></li>';
 }
echo "</ul>";
}
?>
    <div class="row well">
       <legend><label class="default_font_color">Customer Details</label>
<!--           <input type="submit" name="" value="Quote for new customer" class="btn hoverable_btn pull-right" />-->
       </legend>

        <ul class="nav nav-tabs nav-justified main-navs" id="navID">
            <li class="active" ><a href="<?php echo base_url().'customers/adding'; ?>"  aria-expanded="true">New Customer</a></li>
            <li class=""><a href="<?php echo base_url().'customers/listing'; ?>" aria-expanded="false">Select Existing Customer</a></li>
        </ul>

        <div class="tab-content">
            <section class="tab-pane fade in active" id="new">
                <legend>&nbsp</legend>
                <div class="customerAdd">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label>Customer or Company Name <i class="required"> * </i></label>
                            <input name="customerOrCmpnyName" class="form-control" type="text" value="<?php echo set_value('customerOrCmpnyName',$customerOrCmpnyName); ?>">

                        </div>
                        <div class="col-md-6">
                            <label>Contact Person <i class="required"> * </i></label>
                            <input name="contactPersonName" class="form-control" type="text" value="<?php echo set_value('contactPersonName',$contactPersonName); ?>">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Email<i class="required"> * </i></label>
                            <input id="locality" name="cmpnyEmail" class="form-control" value="<?php echo set_value('cmpnyEmail',$cmpnyEmail); ?>" >
                        </div>

                        <div class="col-md-3">
                            <label>Telephone Number <i class="required"> * </i></label>
                            <input name="telNumber" class="form-control" type="text" value="<?php echo set_value('telNumber',$telNumber); ?>">
                        </div>
                        <div class="col-md-3">
                            <label>GST Number</label>
                            <input name="faxNumber" class="form-control" type="text" value="<?php echo set_value('faxNumber',$faxNumber); ?>">
                        </div>
                        <div class="col-md-3">
                            <label>Currency for Customer</label>
                            <select class="form-control" name="customerCurrency" id="customerCurrency">
                                <option value="INR" <?php echo set_select('customerCurrency', 'INR',($customerCurrency == 'INR')); ?>>INR</option>
                                <option value="USD" <?php echo set_select('customerCurrency', 'USD',($customerCurrency == 'USD'));?>>USD</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <label>Shipping Address</label>
                            <textarea name="shippingAddress" class="form-control" ><?php echo $shippingAddress;?></textarea>
                        </div>
                    </div>
                    <legend>&nbsp</legend>
                    <div class="col-md-12 text-center">
                        <?php echo form_submit('save', ' Save', 'class="btn-success btn"'); ?>
<!--                        --><?php //echo form_submit('saveAndNext', ' Save and Next ', 'class="btn hoverable_btn"'); ?>
                        <?php echo form_submit('cancel', ' Cancel ', 'class="btn-danger btn"'); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
