<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

echo form_open_multipart(base_url().'quotations/preview');

if($this->session->flashdata('msg')){
    echo '<div class="alert alert-success row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';
}
if(isset($err_msg)){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';
}
if (isset($msg)) {
    echo '<div class="alert alert-success row"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

if (validation_errors()){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}
?>
<div class="container">
    <?php
    if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
        echo '<ul class="breadcrumb row custom-breadcrumb">';
        $count = count($breadcrumb_array);
        foreach ($breadcrumb_array as $key => $value) {
            if (--$count <= 0) {
                echo "<li class='active'>".$key."</li>";
                break;
            }
            echo '<li><a href='.$value.'>'.$key.'</a></li>';
        }
        echo "</ul>";
    }
    ?>
    <div class="row well">
        <legend><label class="default_font_color">Customer Details</label>
            <!--           <input type="submit" name="" value="Quote for new customer" class="btn hoverable_btn pull-right" />-->
        </legend>
        <ul class="nav nav-tabs nav-justified main-navs" id="navID">
            <li class="active"><a href="<?php echo base_url().'customers/listing'; ?>" aria-expanded="true">Select Existing Customer</a></li>
            <li class="" ><a href="<?php echo base_url().'customers/adding'; ?>"  aria-expanded="false">New Customer</a></li>
        </ul>

        <div class="tab-content">
            <section class="tab-pane fade in active" id="list">
                <div class="custimerlist">
                    <table cellpadding="0" cellspacing="0" border="0" class="display" id="customerDataTable" width="100%">
                        <thead>
                        <tr>
                            <th>Company / customer Name</th>
                            <th>Contact Person</th>
                            <th>Email</th>
                            <th>Telephone</th>
                            <th>GST Number</th>
                            <th>Edit</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </section>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="expencesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add expences and other details for this quotation.</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="nav nav-pills nav-justified">
                            <ul class="nav nav-pills nav-justified" id="navID">
                                <li class="active" id="quotation"><a href="#quotationPanel" data-toggle="tab" aria-expanded="true">
                                        Quotation</a></li>
                                <li class="" id="invoice"><a href="#invoicePanel" data-toggle="tab" aria-expanded="false">Invoice</a></li>
                            </ul>
                            <div class="panel-body threepanel">
                                <div class="tab-content">
                                    <section  class="tab-pane fade in active" id="quotationPanel">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Shipping Charges</label>
                                                <input id="locality" name="qshippingCharges" class="form-control" value="" placeholder="Enter Shipping Charges">
                                            </div>

                                            <div class="col-md-12">
                                                <label>GST charge percentage (%)</label>
                                                <input name="qgstCharges" class="form-control" type="text" value="" placeholder="Enter GST charge in percentage">
                                            </div>
                                            <div class="col-md-12">
                                                <label>Quotation Number</label>
                                                <input name="qquotationNumber" class="form-control" type="text" value="" placeholder="Enter Quotation Number. EX: Quote-001">
                                            </div>
                                            <div class="col-md-12">
                                                <input id="customerId" name="qcustomerId" class="form-control" type="hidden" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <button type="Submit" name="quotation" class="btn btn-primary">Save and proceed</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </section>
                                    <section  class="tab-pane fade in" id="invoicePanel">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Shipping Charges</label>
                                                <input id="locality" name="shippingCharges" class="form-control" value="" placeholder="Enter Shipping Charges">
                                            </div>

                                            <div class="col-md-12">
                                                <label>GST charge percentage (%)</label>
                                                <input name="gstCharges" class="form-control" type="text" value="" placeholder="Enter GST charge in percentage">
                                            </div>
                                            <div class="col-md-12">
                                                <label>Invoice Number</label>
                                                <input name="quotationNumber" class="form-control" type="text" value="" placeholder="Enter Invoice Number. EX: Invoice-001">
                                            </div>
                                            <div class="col-md-12">
                                                <input id="customerId" name="customerId" class="form-control" type="hidden" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <button type="Submit" name="invoice"  class="btn btn-primary">Save and proceed</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
