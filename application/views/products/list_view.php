<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

echo form_open_multipart(base_url().'products', array('id' => 'productValidationForm', 'method' => 'post'));

if($this->session->flashdata('msg')){
    echo '<div class="alert alert-success row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('msg');
    echo '</b></div>';
}
if($this->session->flashdata('errormsg')){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $this->session->flashdata('errormsg');
    echo '</b></div>';
}
if(isset($err_msg)){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $err_msg;
    echo '</b></div>';
}
if (isset($msg)) {
    echo '<div class="alert alert-success row"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo $msg;
    echo '</b></div>';
}

 if (validation_errors()){
    echo '<div class="alert alert-danger row error_msgs"><b><a href="#" class="close" data-dismiss="alert">&times;</a>';
    echo validation_errors();
    echo '</b></div>';
}
 ?>
<div class="body container">
    <?php
    if(isset($breadcrumb_array) && !empty($breadcrumb_array) && is_array($breadcrumb_array)) {
        echo '<ul class="breadcrumb row custom-breadcrumb">';
        $count = count($breadcrumb_array);
        foreach ($breadcrumb_array as $key => $value) {
            if (--$count <= 0) {
                echo "<li class='active'>".$key."</li>";
                break;
            }
            echo '<li><a href='.$value.'>'.$key.'</a></li>';
        }
        echo "</ul>";
    }
    ?>
    <div class="row well">
        <legend>
            <label class="default_font_color">Choose Products and Add Quantity</label>
            <div class="col-md-12 text-center">
                <input id="productValidation"  style="margin-left:20px !important;"  type="submit" name="listingSubmit" value="Save and Proceed" class="productValidation btn hoverable_btn pull-right" />
                <input id="clearProductData" style="margin-left:20px !important;" type="submit" name="listingSubmit" value="Clear product form" class="btn-danger btn pull-right" /> &nbsp&nbsp&nbsp&nbsp
                <a class="btn-success btn pull-right" style="margin-left:20px !important;"  href="<?php echo base_url().'products/add'; ?>">Add products</a>
            </div>
        <legend>&nbsp</legend>
        </legend>
        <input type="hidden" name="fullData" id="fullData" />
    <table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
        <thead>
        <tr>
            <th>Choose products</th>
            <th style="min-width: 150px;">Product Pic</th>
            <th >Product Description</th>
            <th>Details</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Amount</th>
        </tr>
        </thead>
    </table>

        <div class="col-md-12 text-center">
            <label class="">
                <input id="productValidation"  type="submit" name="listingSubmit" value="Save and Proceed" class="productValidation btn hoverable_btn pull-right" /></label>
        </div>
    </div>
</div>