<?php
function lang($word) {
    return ucwords($word);
}
?>
<div class="left_side_bar">
    <div id="wrapper">
        <!-- begin SIDE NAVIGATION -->
        <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <li>
                        <a class="active" href="<?php echo base_url().'company'; ?>">
                            <i class="fa fa-building-o"></i>
                            <?= lang('company profile'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="active" href="<?php echo base_url().'products'; ?>">
                            <i class="fa fa-shopping-cart"></i>
                            <?= lang('Products list'); ?>
                        </a>
                    </li>
                    <!--<li>
                        <a href="<?php /*echo base_url().'quotations'; */?>">
                            <i class="fa fa-list"></i>
                            <?/*= lang('quotations'); */?>
                        </a>
                    </li>-->
                    <li>
                        <a href="<?php echo base_url().'customers/listing/'; ?>">
                            <i class="fa fa-user"></i>
                            <?= lang('customers'); ?>
                        </a>
                    </li>
                </ul>

                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

    </div>
</div>