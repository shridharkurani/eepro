<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('child_model');
        $dbName = $this->session->userdata('userDb');
        $dbArray = $this->child_model->getDbCredentials($dbName);
        $this->child_model->loadDb($dbArray);
        $this->load->model('parent_model');
        LoadCssAndJs($this->layouts);
    }

    function index() {
        $is_logged_in = $this->session->userdata('logged_in');
        if(!$is_logged_in) {
            redirect(base_url());
        }
        $clearProductData = $this->input->get('clearProducts', TRUE);
        if($clearProductData) {
            $sessiondata = array(
                'serializedProductSelection' => array()
            );
            $this->session->set_userdata($sessiondata);
        }
        $data = array();
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $user_id = $this->session->userdata('user_id');
        $cmpnyres = $this->parent_model->getCompanyId($user_id);
        $cmpnyId = (isset($cmpnyres[0]->id_cds)) ? $cmpnyres[0]->id_cds : 0 ;

        $this->_list_set_rules();

        $data['cmpnyId'] = $cmpnyId;
        $data['posted_data'] = $this->input->post();
        $data['cmpnyDetails'] = $this->parent_model->getCompanyDetails($cmpnyId);

        if ($this->form_validation->run() == FALSE) {
            $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(), 'Products List' => ''));
            $this->layouts->view('products/list_view', array('left_sidebar' => 'sidebar/left_sidebar','right_sidebar' => 'sidebar/right_sidebar'),$data,TRUE);
        }
        else {
            $data['posted_data'] = $this->input->post();
            $serializedData = $data['posted_data']['fullData'];

            parse_str($serializedData, $fullData);

            $data['posted_data']['productIds'] = $fullData['productIds;'];
            $data['posted_data']['productDesc'] = $fullData['productDesc;'];
            $data['posted_data']['productUnitPrice'] = $fullData['productUnitPrice;'];
            $data['posted_data']['productQty'] = $fullData['productQty;'];

            $mergedArray = array_merge($fullData['productIds'], $data['posted_data']['productIds']);
            $data['posted_data']['productIds']  = $mergedArray;

            $productSeletionArray = array();
            foreach ($data['posted_data']['productIds'] as $key => $value) {
                if(!empty($data['posted_data']['productQty'][$key]) && !empty($data['posted_data']['productDesc'][$key]) && !empty($data['posted_data']['productUnitPrice'][$key])) {
                    $productSeletionArray[$value] = array(
                        'productDesc' => $data['posted_data']['productDesc'][$key],
                        'unitPrice' => $data['posted_data']['productUnitPrice'][$key],
                        'quantity' => $data['posted_data']['productQty'][$key]
                    );
                }
            }
            if(!empty($productSeletionArray)) {
                $serializedProductSelection = serialize($productSeletionArray);

                $sessiondata = array(
                    'serializedProductSelection' => $serializedProductSelection
                );
                $this->session->set_userdata($sessiondata);
                $error = 0;
            }
            else {
                $error = 1;
                 // Something is wrong
            }
            if(!$error) {
                redirect(base_url().'customers/listing','refresh');
            }
            else {
                $msg = "Something went wrong. Please contact admin.";
                $this->session->set_flashdata('msg', $msg);
                redirect(base_url().'products','refresh');
            }
        }
    }

    function add() {
        $is_logged_in = $this->session->userdata('logged_in');
        if(!$is_logged_in) {
            redirect(base_url());
        }
        $data = array();
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);

        $user_id = $this->session->userdata('user_id');
        $cmpnyId = $this->parent_model->getCompanyId($user_id);
        $this->_add_set_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(), 'Products List' => ''));
            $this->layouts->view('products/add_view', array('left_sidebar' => 'sidebar/left_sidebar','right_sidebar' => 'sidebar/right_sidebar'),$data,TRUE);
        }
        else {

            $data['posted_data'] = $this->input->post();
            if(isset($data['posted_data']['imageUploadSubmit'])) {
                // Images upload logic.

                $this->load->library('upload');
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                $uploadedData = array();
                for($i=0; $i<$cpt; $i++)
                {
                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];

                    $this->upload->initialize($this->set_upload_options());
                    $this->upload->do_upload();
                    $dataInfo = $this->upload->data();
                    $uploadedData[] = array(
                        'file_name_pts' => $dataInfo['file_name'] ,
                        'file_type_pts' => $dataInfo['file_type'] ,
                        'orig_name_pts' => $dataInfo['orig_name'] ,
                        'file_size_pts' => $dataInfo['file_size'] ,
                        'image_type_pts' => $dataInfo['image_type'] ,
                        'image_size_str_pts' => $dataInfo['image_size_str'] ,
                        'is_image_pts' => $dataInfo['is_image'] ,
                        'idcds_pts' => $cmpnyId,
                    );
                }
                $response = $this->parent_model->insertImages($uploadedData);
                if($response['flag']) {
                    $msg = "Products images uploaded Successfully.";
                    $this->session->set_flashdata('msg', $msg);
                    redirect(base_url().'products/add','refresh');
                }
                else {
                    $msg = "Not able to upload images.";
                    $this->session->set_flashdata('errormsg', $msg);
                    redirect(base_url().'products/add','refresh');
                }
            }
            else if(isset($data['posted_data']['listingSubmit'])) {
                print_r($data);
                exit();
            }
        }
    }

    function _edit_set_rules() {
        $this->form_validation->set_rules('cmpnyFullName', 'Company Full Name', 'trim|min_length[3]|xss_clean|required');
        $this->form_validation->set_rules('contactPersonFullName', 'Contact Person Full Name', 'trim|required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('cmpnyEmail', 'Email', 'trim|valid_email|required||xss_clean');
        $this->form_validation->set_rules('telNumber', 'Telephone Number', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('cmpnyEmail', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('userfile', 'Mall Image', 'trim|min_length[3]|xss_clean|callback__verify_uploading_file_and_upload');
    }

    function _add_set_rules() {
        $this->form_validation->set_rules('userfile', 'Product Images', 'trim|xss_clean');
    }

    function _list_set_rules() {
        $this->form_validation->set_rules('listingSubmit', 'Listing Submit', 'trim|xss_clean');
    }

    function set_upload_options() {
        $config['upload_path'] =  'assets/uploads/products';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '2048';
        $config['max_width']  = '0';
        $config['max_height']  = '0';
        $config['encrypt_name'] = TRUE;
        return $config;
    }
}