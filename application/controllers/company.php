<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Company extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('child_model');
        $dbName = $this->session->userdata('userDb');
        $dbArray = $this->child_model->getDbCredentials($dbName);
        $this->child_model->loadDb($dbArray);
        $this->load->model('parent_model');
        LoadCssAndJs($this->layouts);
    }

    function index() {
        $is_logged_in = $this->session->userdata('logged_in');
        if(!$is_logged_in) {
            redirect(base_url());
        }
        $data = array();
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        {
            $user_id = $this->session->userdata('user_id');
            $cmpnyId = $this->parent_model->getCompanyId($user_id);

            $data['cmpnyId'] = $cmpnyId;
            $data['posted_data'] = $this->input->post();
            $data['cmpnyDetails'] = $this->parent_model->getCompanyDetails($cmpnyId);

            if($user_id && $cmpnyId){
                $isUpdate = true;
            }
            else {
                $isUpdate = false;
                $inputs_array['created_by_cds'] = $user_id;;
            }

            $this->_edit_set_rules($isUpdate);


            if ($this->form_validation->run() == FALSE) {
                $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(), 'Company Profile' => ''));
                $this->layouts->view('company/add_edit_view', array('left_sidebar' => 'sidebar/left_sidebar','right_sidebar' => 'sidebar/right_sidebar'),$data,TRUE);
            }
            else {
                //Save and redirect..
                $inputs_array = array(
                    'cmpnyname_cds' => $data['posted_data']['cmpnyFullName'],
                    'cmpny_contact_person_cds' => $data['posted_data']['contactPersonFullName'],
                    'cmpnyemail_cds' => $data['posted_data']['cmpnyEmail'],
                    'cmpnymobile_cds' => $data['posted_data']['telNumber'],
                    'cmpnyfax_cds' => $data['posted_data']['faxNumber'],
                    'price_terms_cds' => $data['posted_data']['priceTerms'],
                    'payment_terms_cds' => $data['posted_data']['paymentTerms'],
                    'quotation_validity_cds' => $data['posted_data']['quotationValidity'],
                    'note_cds' => $data['posted_data']['note'],
                    'idusr_cds' => $user_id,
                    'created_by_cds' => $user_id,
                );


                if(isset($this->filedata) && is_array($this->filedata)) {
                    $inputs_array['cmpny_logo_cds'] = $this->filedata['file_name'];
                }

                $response = $this->parent_model->insert_update($inputs_array,'companydetails_cds',$isUpdate,$cmpnyId);
//                print_r($response);exit();
                if($response['msg']) {
                    $msg = "Record ".$response['flag']. " Successfull.";
                    $this->session->set_flashdata('msg', $msg);
                    redirect(base_url().'company','refresh');
                }
                else {
                    $msg = "Not able to ".$response['flag']. " record.";
                    $this->session->set_flashdata('msg', $msg);
                    redirect(base_url().'company','refresh');
                }
            }
        }
    }

    function _edit_set_rules($isUpdate = 0) {
        $this->form_validation->set_rules('cmpnyFullName', 'Company Full Name', 'trim|min_length[3]|xss_clean|required');
        $this->form_validation->set_rules('contactPersonFullName', 'Contact Person Full Name', 'trim|required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('cmpnyEmail', 'Email', 'trim|valid_email|required||xss_clean');
        $this->form_validation->set_rules('telNumber', 'Telephone Number', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('faxNumber', 'Fax Number', 'trim|required|xss_clean');
        if($isUpdate == true) {
            $this->form_validation->set_rules('userfile', 'Company logo1', 'trim|xss_clean|callback__verify_uploading_file_and_upload');
        }
        else {
            if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name']))
            $this->form_validation->set_rules('userfile', 'Company logo2', 'trim|xss_clean|callback__verify_uploading_file_and_upload');
        }
    }

    function _verify_uploading_file_and_upload() {
        $config['upload_path'] =  'assets/uploads';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '2048';
        $config['max_width']  = '0';
        $config['max_height']  = '0';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile'))
        {

            if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])){
                $error = $this->upload->display_errors();
                $this->form_validation->set_message('_verify_uploading_file_and_upload',$error);
                return false;
            }
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $this->filedata = $data['upload_data'];
            return true;
        }
    }
}