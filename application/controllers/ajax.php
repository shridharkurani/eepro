<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends CI_Controller {
	
	public function __construct()
	{
        parent::__construct();
        $this->load->model('child_model');
        /*TODO*/
        $dbName = $this->session->userdata('userDb');
        $dbArray = $this->child_model->getDbCredentials($dbName);
        $this->child_model->loadDb($dbArray);
        
        $this->load->model('parent_model');
	}
	function confitmQuotation() {
	    $postData = $this->input->post();
        $input_array = array(
            'fullurl_qts' => $postData['url'],
            'idcds_qts' => $postData['companyID'],
            'datetime_qts' => date('Y-m-d H:i:s'),
        );

        $msg = $this->parent_model->insert_update($input_array, "quotations_qts", 0, 0) ;
	    echo json_encode($msg);
	}

	function clearProductSessionData() {
        $sessiondata = array(
            'serializedProductSelection' => array()
        );
        $this->session->set_userdata($sessiondata);
    }

    function get_products($default = 'child') {
        if($this->input->is_ajax_request()){
            if($default == 'parent') {
                $user_id = $this->session->userdata('user_id');
                $cmpnyId = $this->parent_model->getCompanyId($user_id);
                $returned_array['data'] = $this->parent_model->getProducts($cmpnyId);
            }
            else {
                $returned_array['data'] = $this->child_model->getProducts();
            }
            $rawSessionProductData = $this->session->userdata('serializedProductSelection');
            if(!empty($rawSessionProductData)) {
                $sessionProductData = unserialize($this->session->userdata('serializedProductSelection'));
                foreach ($sessionProductData as $key => $value) {
                    foreach ($returned_array['data'] as $retkey => $retvalue) {
                        if($returned_array['data'][$retkey]['productId'] == $key) {
                            $returned_array['data'][$retkey]['productPrice'] = $value['unitPrice'];
                            $returned_array['data'][$retkey]['productDesc'] = $value['productDesc'];
                            $returned_array['data'][$retkey]['quantity'] = $value['quantity'];
                            $returned_array['data'][$retkey]['productIsChecked'] = 1 ;
                            $returned_array['data'][$retkey]['amount'] = $value['unitPrice'] * $value['quantity'];

                        }
                    }
                 }
            }
            /*echo "<pre>";
            $sessionProductData = unserialize($this->session->userdata('serializedProductSelection'));

            print_r($sessionProductData);
            print_r($returned_array);
            exit;*/
            echo json_encode($returned_array, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }
        else {
            redirect(base_url(),"refresh");
        }
    }
    function get_all_customers() {
        if($this->input->is_ajax_request()){
            /*TODO*/
            $user_id = $this->session->userdata('user_id');
            $cmpnyId = $this->parent_model->getCompanyId($user_id);

            $returned_array['data'] = $this->parent_model->getAllCustomerDetails($cmpnyId);
            echo json_encode($returned_array, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }
        else {
            redirect(base_url(),"refresh");
        }
    }
}