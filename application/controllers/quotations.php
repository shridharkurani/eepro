<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Quotations extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('child_model');
        $dbName = "kannada_first"; //$this->session->userdata('userDb');
        $dbArray = $this->child_model->getDbCredentials($dbName);
        $this->child_model->loadDb($dbArray);
        $this->load->model('parent_model');
        LoadCssAndJs($this->layouts);
    }
    function GeneratePdf(){
        $this->layouts->view('quotations/pdf_view', array('left_sidebar' => 'sidebar/left_sidebar','right_sidebar' => 'sidebar/right_sidebar'),$data,TRUE);

//        $this->load->view('welcome_message');
        $html = $this->output->get_output();
        // Load pdf library
        $this->load->library('pdf');
        $this->pdf->loadHtml($html);
        $this->pdf->setPaper('A4', 'landscape');
        $this->pdf->render();
        // Output the generated PDF (1 = download and 0 = preview)
        $this->pdf->stream("html_contents.pdf", array("Attachment"=> 0));
    }

    function preview($customerId = 0) {
        $productSeletionDetails = $this->session->userdata('serializedProductSelection');
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $extraDetails = $this->input->post();
        $customerId = $extraDetails['customerId'];

        if(isset($extraDetails['quotation'])) {
            // Quotation
            $extraDetails['shippingCharges'] = $extraDetails['qshippingCharges'];
            $extraDetails['gstCharges'] = $extraDetails['qgstCharges'];
            $extraDetails['quotationNumber'] = $extraDetails['qquotationNumber'];
            $data['quotationOrInvoiceString'] = "Quotation";
        }
        else {
            // Invoice
            $data['quotationOrInvoiceString'] = "Invoice";
        }

        if(!empty($productSeletionDetails)) {
            $productIds = array();
            $qtyArray = array();
            if($customerId) {
                $companyID = $this->parent_model->getCustomerCompanyId($customerId);

                $serializedProductSelection = $this->session->userdata('serializedProductSelection');
//                $prodcutSelections = unserialize($serializedProductSelection);
//                $prodctsAndQty = json_decode($getData['products'], true);
                $prodctsAndQty = unserialize($serializedProductSelection);
                $data['extraDetails'] = $extraDetails;
                $data['companyAndUserDetails'] = $this->parent_model->getCompanyDetails($companyID);
                /*$quotationPreString = $this->config->item($this->session->userdata('userDb'))['quotationPreString'];
                $data['quotationNumber'] = $quotationPreString . $this->parent_model->getQuotationNumber($companyID);*/
                $data['companyDbName'] = $data['companyAndUserDetails']['databasename_usr'];
                $data['customerdetails'] = $this->parent_model->getCustomerDetails($customerId);
                $currency = !empty($data['customerdetails']['customer_currency_cus']) ? $data['customerdetails']['customer_currency_cus'] : 'INR';
                $data['companyId'] = $companyID;

                $this->layouts->set_title("EEPRO");
                $this->layouts->set_description(SITE_NAME);

                $companyName = $this->session->userdata('userDb');

               /* foreach ($prodctsAndQty as $key => $product) {
                    $productId = substr($product['Id'], 7, 10);
                    if(substr($product['Id'],0,7) == 'product' && $product['quantity'] > 0) {
                        $productIds[] = $productId;
                        $qtyArray[$productId]['qty'] = $product['qty'];
                        $qtyArray[$productId]['rawprice'] = $product['p'];
                    }
                }*/
                foreach ($prodctsAndQty as $key => $product) {
                    $productId = $key;
                    $productIds[] = $productId;
                    $qtyArray[$productId]['qty'] = $product['quantity'];
                    $qtyArray[$productId]['rawprice'] = $product['unitPrice'];
                    $qtyArray[$productId]['productDesc'] = $product['productDesc'];
                }
                $data['productDetails'] = $this->child_model->getProducts($productIds,1);

                $netTotal = 0;
                foreach ($data['productDetails'] as $key => $product) {
                    $qtyArray[$product['productId']]['unitprice'] = $this->child_model->setCurrency($qtyArray[$product['productId']]['rawprice'] , $currency);
                    $qtyArray[$product['productId']]['price'] = $this->child_model->setCurrency($qtyArray[$product['productId']]['qty'] * $qtyArray[$product['productId']]['rawprice'] , $currency);
                    $qtyArray[$product['productId']]['rawunitprice'] = (($qtyArray[$product['productId']]['qty']) * ($qtyArray[$product['productId']]['rawprice']));
                    $netTotal += $qtyArray[$product["productId"]]['rawunitprice'];
                }
                $data['qtyArray'] = $qtyArray;
                $data['netArray']['netTotal'] = $this->child_model->setCurrency($netTotal, $currency);
                $data['netArray']['shippingCharges'] = $this->child_model->setCurrency($extraDetails['shippingCharges'], $currency);
                $data['netArray']['gst'] = $this->child_model->setCurrency(($netTotal * $extraDetails['gstCharges']) / 100, $currency);
                $data['netArray']['subTotal'] = $this->child_model->setCurrency(($netTotal * $extraDetails['gstCharges']) / 100, $currency);
                $totalAmount = ($netTotal + (($netTotal * $extraDetails['gstCharges']) / 100)) + $extraDetails['shippingCharges'];
                $data['netArray']['grandTotal'] = $this->child_model->setCurrency($totalAmount, $currency);

                if ($this->form_validation->run() == FALSE) {
                    $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(), 'Company Profile' => ''));
                    $this->layouts->view('quotations/preview_view', array('left_sidebar' => 'sidebar/left_sidebar','right_sidebar' => 'sidebar/right_sidebar'),$data,TRUE);
                }
                else {
                    // Generated and sharable.
                }
            }
            else {
                $msg = "Please choose customers";
                $this->session->set_flashdata('errormsg', $msg);
                redirect(base_url().'customers');
            }
        }
        else {
            $msg = "Please choose products";
            $this->session->set_flashdata('errormsg', $msg);
            redirect(base_url().'products');
        }
    }

    function _edit_set_rules() {
        $this->form_validation->set_rules('cmpnyFullName', 'Company Full Name', 'trim|min_length[3]|xss_clean|required');
        $this->form_validation->set_rules('contactPersonFullName', 'Contact Person Full Name', 'trim|required|min_length[3]|xss_clean');
        $this->form_validation->set_rules('cmpnyEmail', 'Email', 'trim|valid_email|required||xss_clean');
        $this->form_validation->set_rules('telNumber', 'Telephone Number', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('faxNumber', 'Fax Number', 'trim|required|xss_clean');
        $this->form_validation->set_rules('userfile', 'Mall Image', 'trim|min_length[3]|xss_clean|callback__verify_uploading_file_and_upload');
    }

    function _verify_uploading_file_and_upload() {
        $config['upload_path'] =  'assets/uploads';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '2048';
        $config['max_width']  = '0';
        $config['max_height']  = '0';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $error = $this->upload->display_errors();
            $this->form_validation->set_message('_verify_uploading_file_and_upload',$error);
            return false;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $this->filedata = $data['upload_data'];
            return true;
        }
    }
}