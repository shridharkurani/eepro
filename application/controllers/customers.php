<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('child_model');
        $dbName = "kannada_first"; //$this->session->userdata('userDb');
        $dbArray = $this->child_model->getDbCredentials($dbName);
        $this->child_model->loadDb($dbArray);
        $this->load->model('parent_model');
        LoadCssAndJs($this->layouts);
    }
    function index()
    {
        redirect($this->listing());
    }

    function listing($custmerId = 0) {

        $is_logged_in = $this->session->userdata('logged_in');

        if(!$is_logged_in) {
            redirect(base_url());
        }
        $data = array();
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $user_id = $this->session->userdata('user_id');
        $cmpnyId = $this->parent_model->getCompanyId($user_id);

        $data['cmpnyId'] = $cmpnyId;
        $data['custmerId'] = $custmerId;
        $data['posted_data'] = $this->input->post();

        $this->session->set_flashdata('productSeletionDetails', $data['posted_data']);

        $this->_list_set_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(), 'Company Profile' => ''));
            $this->layouts->view('customers/list_view', array('left_sidebar' => 'sidebar/left_sidebar','right_sidebar' => 'sidebar/right_sidebar'),$data,TRUE);
        }
        else {
            //Save and redirect..
            echo "<pre>";
            print_r($this->session->flashdata('productSeletionDetails'));
            exit();
            redirect(base_url().'quotations/preview/'.$insertedCustomerId.'?'.$getData['products']);

            $inputs_array = array(
                'company_or_customer_name_cus' => $data['posted_data']['customerOrCmpnyName'],
                'fullname_cus' => $data['posted_data']['contactPersonName'],
                'email_cus' => $data['posted_data']['cmpnyEmail'],
                'mobile_cus' => $data['posted_data']['telNumber'],
                'fax_cus' => $data['posted_data']['faxNumber'],
                'customer_currency_cus' => $data['posted_data']['customerCurrency'],
                'shipping_address_cus' => $data['posted_data']['shippingAddress'],
            );

            if($user_id && $custmerId){
                $isUpdate = true;
            }
            else {
                $isUpdate = false;
                $inputs_array['idcds_cus'] = $cmpnyId;;
            }
            $response = $this->parent_model->insert_update($inputs_array,'customer_details_cus',$isUpdate,$custmerId);
//                print_r($response);exit();
            $insertedCustomerId = $response['msg'];
            if($insertedCustomerId) {
                $msg = "Record ".$response['flag']. " Successfull";
                $this->session->set_flashdata('msg', $msg);
                if(isset($data['posted_data']['save'])) {
                    redirect(base_url().'customers/list/'.$insertedCustomerId,'refresh');
                }
                else if($data['posted_data']['saveAndNext']) {
                    redirect(base_url().'quotations/preview/'.$insertedCustomerId.'?'.$getData['products']);
                }
            }
            else {
                $msg = "Not able to ".$response['flag']. " records";
                $this->session->set_flashdata('msg', $msg);
                redirect(base_url().'customers/list/'.$custmerId,'refresh');
            }
        }
    }

    function adding($custmerId = 0)  {

        $is_logged_in = $this->session->userdata('logged_in');
//        $postData = $this->input->post();

        /* if(!$is_logged_in) {
             redirect(base_url());
         }*/
        $data = array();
        $this->layouts->set_title(SITE_NAME);
        $this->layouts->set_description(DEFAULT_SITE_DESC);
        $user_id = $this->session->userdata('user_id');
        $cmpnyId = $this->parent_model->getCompanyId($user_id);

        $this->_add_edit_set_rules();

        $data['cmpnyId'] = $cmpnyId;
        $data['custmerId'] = $custmerId;
        $data['posted_data'] = $this->input->post();

        /*echo "<pre>";
        print_r($data);
        exit();*/

        if($custmerId) {
            $data['customerDetails'] = $this->parent_model->getCustomerDetails($custmerId);
        }
        if(isset($data['posted_data']['cancel'])) {
            redirect(base_url().'customers/list/'.$custmerId,'refresh');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->layouts->set_breadcrumb_array(array('<span class="fa fa-home"></span>' => base_url(), 'Company Profile' => ''));
            $this->layouts->view('customers/add_edit_view', array('left_sidebar' => 'sidebar/left_sidebar','right_sidebar' => 'sidebar/right_sidebar'),$data,TRUE);
        }
        else {
            //Save and redirect..
            $inputs_array = array(
                'company_or_customer_name_cus' => $data['posted_data']['customerOrCmpnyName'],
                'fullname_cus' => $data['posted_data']['contactPersonName'],
                'email_cus' => $data['posted_data']['cmpnyEmail'],
                'mobile_cus' => $data['posted_data']['telNumber'],
                'fax_cus' => $data['posted_data']['faxNumber'],
                'customer_currency_cus' => $data['posted_data']['customerCurrency'],
                'shipping_address_cus' => $data['posted_data']['shippingAddress'],
            );

            if($user_id && $custmerId){
                $isUpdate = true;
            }
            else {
                $isUpdate = false;
                $inputs_array['idcds_cus'] = $cmpnyId;;
            }
            $response = $this->parent_model->insert_update($inputs_array,'customer_details_cus',$isUpdate,$custmerId);
//                print_r($response);exit();
            $insertedCustomerId = $response['msg'];
            if($insertedCustomerId) {
                $msg = "Record ".$response['flag']. " Successfull";
                $this->session->set_flashdata('msg', $msg);
                if(isset($data['posted_data']['save'])) {
                    redirect(base_url().'customers/listing/'.$insertedCustomerId,'refresh');
                }
                else if($data['posted_data']['saveAndNext']) {
                    redirect(base_url().'quotations/preview/'.$insertedCustomerId.'?'.$getData['products']);
                }
            }
            else {
                $msg = "Not able to ".$response['flag']. " records";
                $this->session->set_flashdata('msg', $msg);
                redirect(base_url().'customers/list/'.$custmerId,'refresh');
            }
        }
    }

    function _add_edit_set_rules() {
        $this->form_validation->set_rules('customerOrCmpnyName', 'Customer or Company Name', 'trim|min_length[3]|xss_clean|required');
        $this->form_validation->set_rules('contactPersonName', 'Contact Person', 'trim|min_length[3]|xss_clean|required');
        $this->form_validation->set_rules('cmpnyEmail', 'Email', 'trim|valid_email|required||xss_clean');
        $this->form_validation->set_rules('telNumber', 'Telephone Number', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('faxNumber', 'GST Number', 'trim|xss_clean');
        $this->form_validation->set_rules('customerCurrency', 'Customer Currency', 'trim|xss_clean|required');
    }
    function _list_set_rules() {
        $this->form_validation->set_rules('listingSubmit', 'Listing Submit', 'trim|xss_clean');
    }
}