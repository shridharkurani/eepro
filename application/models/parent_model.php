<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class parent_model extends CI_Model
{
    private $childDb;

    function __construct()
    {
        parent::__construct();
    }

    function getCompanyDetails($cmpnyId){
        $this->db->select('*');
        $this->db->from("companydetails_cds cds");
        $this->db->join('user_usr usr', 'cds.idusr_cds = usr.id_usr', 'left');
        $this->db->where(array('cds.id_cds' => $cmpnyId));
        $query = $this->db->get();
        return $query->row_array();
    }

    function getCustomerDetails($customerId){
        $this->db->select('*');
        $this->db->from("customer_details_cus cus");
        $this->db->join('companydetails_cds cds', 'cds.id_cds = cus.idcds_cus', 'left');
        $this->db->order_by('cus.id_cus', 'asc');
        $this->db->where(array('cus.id_cus' => $customerId));
        $query = $this->db->get();
        return $query->row_array();
    }

    function getAllCustomerDetails($companyId){
        $this->db->select('*');
        $this->db->from("customer_details_cus cus");
        $this->db->join('companydetails_cds cds', 'cds.id_cds = cus.idcds_cus', 'left');
        $this->db->order_by('cus.id_cus', 'desc');
        $this->db->where(array('cds.id_cds' => $companyId));
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_valid_user($email_or_mobile, $table_name) {
        if (filter_var($email_or_mobile, FILTER_VALIDATE_EMAIL)) {
            $this->db->select('*');
            $this->db->from($table_name);
            $this->db->where(array('email_usr' => $email_or_mobile));
            $query = $this->db->get();
            return ($query) ? $query->row_array() : array();

        }
        else {
            $this->db->select('*');
            $this->db->from($table_name);
            $this->db->where(array('mobil_usr' => $email_or_mobile));
            $this->db->where("mobile_usr !=", 0 );
            $query = $this->db->get();
            return ($query) ? $query->row_array() : array();
        }
    }

    function insert_update($input_array, $tableName, $isUpdate, $primaryKeyVal = 0) {
        $primaryKey = $this->getMapper($tableName);
        if($isUpdate && $primaryKeyVal) { // update
            $this->db->where($primaryKey,$primaryKeyVal);
            $query = $this->db->update($tableName, $input_array);
            return array('flag' => 'Update','msg' => ($query) ? true : false);
        }
        else {
            $this->db->insert($tableName, $input_array);
            /*print_r($tableName);
            print_r($input_array);
            print_r($this->db->insert_id());
            echo "Affected row";
            var_dump($this->db->affected_rows());
            return ($this->db->last_query());exit();*/

            return array('flag' => 'Insert','msg' => ($this->db->affected_rows() != 1) ? false : $this->db->insert_id());
        }
    }
    function insertImages($dataArr) {
        $this->db->insert_batch('products_pts', $dataArr);
        /*echo "<pre>";
        print_r($this->db);
        exit();*/
        if($this->db->affected_rows() > 0)
            return array('flag' => 1);
        else
            return array('flag' => 0);
    }

    function getProducts($cmpnyId)
    {
        $this->db->select("pts.created_time_pts as created, pts.name_pts as productDesc, pts.file_name_pts as productImageFileName, pts.id_pts as productId");
        $this->db->from("products_pts pts");
//        $this->db->join('products_pts pts', 'pts.id_pts = cds.id_cds', 'left');
//        $this->db->join('product_images_pim pim', 'pim.id_pim = pts.idpim_pts', 'right');
        $this->db->where(array('pts.is_image_pts' => 1, 'pts.flag_pts' => 1, 'pts.idcds_pts' => $cmpnyId));
        $this->db->order_by('pts.created_time_pts', 'asc');
        $query = $this->db->get();

        $processedArray = $this->processarray($query->result_array());
        return $processedArray;
    }

    function processarray($products) {

        for($i = 0; $i < count($products); $i++) {
            $products[$i]['productPrice'] = 0;
            $products[$i]['quantity'] = 0;
            $products[$i]['productIsChecked'] = 0;
            $products[$i]['amount'] = 0;
            $products[$i]['productImg'] = asset_url().'uploads/products/'.$products[$i]['productImageFileName'];
            $products[$i]['productDesc'] = $products[$i]['productDesc'];
            $products[$i]['created'] = date('d/m/yy h:i A', strtotime($products[$i]['created']));
        };
        return $products;
    }

    function getMapper($tablename) {
        $idArray = array(
            'companydetails_cds' => 'id_cds',
            'customer_details_cus' => 'id_cus',
            'quotations_qts' => 'id_qts',
        );
        return $idArray[$tablename];
    }

    function getCompanyId($userId) {
        $this->db->select("cds.id_cds");
        $this->db->from("companydetails_cds cds");
        $this->db->join('user_usr usr', 'cds.idusr_cds = usr.id_usr', 'right');
        $this->db->where(array('usr.id_usr' => $userId));
        $query = $this->db->get();
        return $query->row(0)->id_cds;
    }

    function getCustomerCompanyId($customerId) {
        $this->db->select("cus.idcds_cus");
        $this->db->from("companydetails_cds cds");
        $this->db->join('customer_details_cus cus', 'cus.idcds_cus = cds.id_cds', 'right');
        $this->db->where(array('cus.id_cus' => $customerId));
        $query = $this->db->get();
        return $query->row(0)->idcds_cus;
    }

    function getQuotationNumber($cmpnyId) {
        $this->db->select("count(id_qts) as quotationNumber");
        $this->db->from("quotations_qts qts");
        $this->db->where(array('qts.idcds_qts' => $cmpnyId));
        $query = $this->db->get();
        return $query->row(0)->quotationNumber + 1;
    }
}