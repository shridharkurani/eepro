<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class child_model extends CI_Model
{
    private $childDb;

    function __construct()
    {
        
        parent::__construct();
        
   $db2 = $this->load->database('database2', TRUE);
        $this->loadDb(array(
            'hostname' => 'localhost',
            'username' => 'shreelea_wp685',
            'password' => '6g4S27[p-q',
            'database' => 'shreelea_wp685',
            ));
            
    }

    function loadDb($configArr)
    {
        $config_app['hostname'] = $configArr['hostname'];
        $config_app['username'] = $configArr['username'];
        $config_app['password'] = $configArr['password'];
        $config_app['database'] = $configArr['database'];
        $config_app['dbdriver'] = 'mysqli';
        $config_app['dbprefix'] = '';
        $config_app['pconnect'] = FALSE;
        $config_app['db_debug'] = FALSE;
        $this->childDb = $this->load->database($config_app, TRUE);
    }

    function getProducts($idsArray = array(),$isRaw = 0)
    {
        $this->childDb = $this->load->database('database2', TRUE);
        
        $defaultWhereArray = array('post_type' => "product", 'post_status' => 'publish');

        $this->childDb->select('ID as productId,post_title as productName');
        $this->childDb->from('wp_posts');
        $this->childDb->where($defaultWhereArray);
        if(!empty($idsArray)) {
            $this->childDb->where_in('ID' , $idsArray);
        }
        // return $this->childDb->last_query();
        $query = $this->childDb->get();
        $processedArray = $this->processarray($query->result_array(), $isRaw);
        return $processedArray;
    }
    function processarray($products, $isRaw) {
        for($i = 0; $i < count($products); $i++) {
            $products[$i]['productPrice'] = $this->getProductPrice($products[$i]['productId']);
            $products[$i]['quantity'] = 0;
            $products[$i]['productIsChecked'] = 0;
            $products[$i]['amount'] = 0;
            $products[$i]['productImg'] = $this->getProductThumbnailImg($products[$i]['productId']);
//            $products[$i]['productDesc'] = preg_replace('/\s/',' ',$products[$i]['productDesc']);
            $products[$i]['productDesc'] = '';//substr($products[$i]['productDesc'],0,200 ). '...';
            if(!$isRaw) {
                $products[$i]['productPrice'] =  $this->setCurrency($products[$i]['productPrice'], '');
            }
        };
        return $products;
    }

    function setCurrency($amount, $currency = 'INR'){
        if(!empty($currency)) {
            $currencyArr['INR'] = array('symbol' => '&#8377;', 'locale' => 'en_IN');
            $currencyArr['USD'] = array('symbol' => '&#36;', 'locale' => 'en_US');
            $fmt = new NumberFormatter($locale = $currencyArr[$currency]['locale'], NumberFormatter::DECIMAL);
            return $currencyArr[$currency]['symbol'] . ' ' . $fmt->format($amount);
        }
        else {
            return $amount;
        }
    }
    function getProductPrice($postId) {
        $this->childDb = $this->load->database('database2', TRUE);
        $this->childDb->select('meta_value as productPrice');
        $this->childDb->from('wp_postmeta');
        $this->childDb->where(array('meta_key' => "_sale_price", 'post_id' => $postId));
        $query = $this->childDb->get();
        /*TODO*/ // For existing price table.
        if(! $query) {
            // zero result.
            return 0;
        }
        else {
            return 0;
            // return $query->row(0)->productPrice;
        }
    }
    function getProductThumbnailImg($postId) {
        $this->childDb = $this->load->database('database2', TRUE);

        $this->childDb->select("wp.guid as imageUrl");
        $this->childDb->from("wp_postmeta wpm");
        $this->childDb->join('wp_posts wp', 'wpm.meta_value = wp.ID', 'left');
        $this->childDb->where(array('wpm.post_id' => $postId, 'wpm.meta_key' => '_thumbnail_id', 'wp.post_type' => 'attachment'));
        $query = $this->childDb->get();
        $imageUrl = (!empty($query->row(0))) ? $query->row(0)->imageUrl : '';
        return str_replace("\\/", "", $imageUrl);
//        return stripslashes($query->row(0)->imageUrl);

    }
    function getDbCredentials($companyName = 'kannada_first') {
        return $this->config->item($companyName);

    }
}