-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 05, 2021 at 06:19 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `companydetails_cds`
--

DROP TABLE IF EXISTS `companydetails_cds`;
CREATE TABLE IF NOT EXISTS `companydetails_cds` (
  `id_cds` int(11) NOT NULL AUTO_INCREMENT,
  `cmpnyname_cds` varchar(100) NOT NULL,
  `cmpny_contact_person_cds` varchar(30) NOT NULL,
  `cmpnyfax_cds` varchar(12) NOT NULL,
  `cmpnymobile_cds` varchar(15) NOT NULL,
  `cmpnyemail_cds` varchar(30) NOT NULL,
  `cmpny_logo_cds` varchar(100) NOT NULL,
  `cmpny_address_cds` varchar(500) DEFAULT NULL,
  `idusr_cds` int(11) NOT NULL,
  `created_by_cds` int(11) NOT NULL,
  `created_date_cds` datetime DEFAULT NULL,
  `last_modified_date_cds` timestamp NOT NULL DEFAULT current_timestamp(),
  `flag_cds` tinyint(1) NOT NULL DEFAULT 1,
  `price_terms_cds` text NOT NULL,
  `payment_terms_cds` text NOT NULL,
  `quotation_validity_cds` text NOT NULL,
  `note_cds` tinytext NOT NULL,
  PRIMARY KEY (`id_cds`),
  UNIQUE KEY `id_cds` (`id_cds`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companydetails_cds`
--

INSERT INTO `companydetails_cds` (`id_cds`, `cmpnyname_cds`, `cmpny_contact_person_cds`, `cmpnyfax_cds`, `cmpnymobile_cds`, `cmpnyemail_cds`, `cmpny_logo_cds`, `cmpny_address_cds`, `idusr_cds`, `created_by_cds`, `created_date_cds`, `last_modified_date_cds`, `flag_cds`, `price_terms_cds`, `payment_terms_cds`, `quotation_validity_cds`, `note_cds`) VALUES
(1, 'My first company', 'sjkurani123', '1234123', '7899452123', 'sjkurani@gmail.co', '6af9889b66c390b631c152c92f3f75ab.PNG', '#bangalore , 123', 1, 1, '2020-07-23 00:00:00', '2020-07-22 18:30:00', 1, '<p>- Above prices is valid for two weeks only.</p>\r\n<p>- Price can be negotiated if the order qty is over 100,000pcs per order as per the actual updated situation. Delivery and payment term by TT : Stock available</p>\r\n<p>- 2/3 daysafter 50% payment be receivedandthebalance be paiddelivery.By normalproduction</p>\r\n<p>- 5-7 day after 50% payment be received and the balance be paid before delivery.</p>\r\n<p>- Service : QC : We would submit you the QC report for each shipment before delivery and make sure all the production good quality - Logistic : We can offer you with one stop service from production to DDP logistic service.</p>', '<p>this is payment terms123</p>', '<p>TOTAL&nbsp;BRAND&nbsp;SOLUTION. LTD.</p>\r\n<p>2nd cross&nbsp;, 3rd building</p>\r\n<p>&nbsp;</p>', '<p>Total Brand Solution Ent. Ltd. reserve the right to change all prices and term without prior notice after cost valdations.</p>'),
(6, 'Onemove software solutions', 'shridhar kurani', 'test fax', '7897897989', 'admin@eepro.in', 'cd29623ee2b1bfb6b3987e97e77ac1cf.jpg', NULL, 3, 3, NULL, '2020-08-01 12:58:52', 1, '<p>- Above prices is valid for two weeks only. -</p>\r\n<p>Pricecan be negotiated if theorderqty is over 100,000pcs perorder as pertheactualupdatedsituation. Deliveryandpaymentterm by TT : Stockavailable</p>\r\n<p>- 2/3 daysafter 50% payment be receivedandthebalance be paiddelivery.By normalproduction</p>\r\n<p>- 5-7 day after 50% payment be received and the balance be paid before delivery.</p>\r\n<p>- Service : QC : We would submit you the QC report for each shipment before delivery and make sure all the production good quality - Logistic : We can offer you with one stop service from production to DDP logistic service</p>\r\n<p>&nbsp;</p>', '<p>-&nbsp;Above&nbsp;prices&nbsp;is&nbsp;valid&nbsp;for&nbsp;two&nbsp;weeks&nbsp;only. -</p>\r\n<p>Pricecan&nbsp;be&nbsp;negotiated&nbsp;if&nbsp;theorderqty&nbsp;is&nbsp;over&nbsp;100,000pcs&nbsp;perorder&nbsp;as&nbsp;pertheactualupdatedsituation.&nbsp;Deliveryandpaymentterm&nbsp;by TT :&nbsp;Stockavailable</p>\r\n<p>- 2/3&nbsp;days&nbsp;after&nbsp;50%&nbsp;payment&nbsp;be&nbsp;received&nbsp;and&nbsp;the&nbsp;balance&nbsp;be&nbsp;paid&nbsp;delivery.By&nbsp;normal&nbsp;production</p>\r\n<p>- 5-7&nbsp;day&nbsp;after&nbsp;50%&nbsp;payment&nbsp;be&nbsp;received&nbsp;and&nbsp;the&nbsp;balance&nbsp;be&nbsp;paid&nbsp;before&nbsp;delivery.</p>\r\n<p>&nbsp;</p>\r\n<p>-&nbsp;Service&nbsp;: QC : We&nbsp;would&nbsp;submit&nbsp;you&nbsp;the&nbsp;QC&nbsp;report&nbsp;for&nbsp;each&nbsp;shipment&nbsp;before&nbsp;delivery&nbsp;and&nbsp;make&nbsp;sure&nbsp;all&nbsp;the&nbsp;production&nbsp;good&nbsp;quality&nbsp;-&nbsp;Logistic&nbsp;: We&nbsp;can&nbsp;offer&nbsp;you&nbsp;with&nbsp;one&nbsp;stop&nbsp;service&nbsp;from&nbsp;production&nbsp;to&nbsp;DDP&nbsp;logistic&nbsp;service</p>', '<p>Onemove software soltioitn LLP 2nd Cross banashakari. Bangalore -560100</p>', '<p>This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE . This is NOTE .</p>'),
(7, 'Shreeleaf', 'Sairam', 'Fax1', '9341217091', 'info@shreeleaf.com', 'cbfa599be22b8804520443faf6e45991.jpg', NULL, 4, 4, NULL, '2020-08-01 18:19:14', 1, '<p>- Above prices is valid for two weeks only.&nbsp;</p>\r\n<p>- Price can be negotiated if the order qty is over 100,000pcs per order as per the actual updated situation. Delivery and payment term by TT : Stock available&nbsp;</p>\r\n<p>- 2/3 days after 50% payment be received and the balance be paid delivery.By normal production&nbsp;</p>\r\n<p>- 5-7 day after 50% payment be received and the balance be paid before delivery.&nbsp;</p>\r\n<p>- Service : QC : We would submit you the QC report for each shipment before delivery and make sure all the production good quality&nbsp;</p>\r\n<p>- Logistic : We can offer you with one stop service from production to DDP logistic service</p>', '', '<p>No.109 , 2nd Block, 5th Cross&nbsp;</p>\r\n<p>Beside Kendriya Sadan ,</p>\r\n<p>(Opp. Vishweshwaraiah Guest House), Koramangala&nbsp;</p>', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer_details_cus`
--

DROP TABLE IF EXISTS `customer_details_cus`;
CREATE TABLE IF NOT EXISTS `customer_details_cus` (
  `id_cus` int(11) NOT NULL AUTO_INCREMENT,
  `company_or_customer_name_cus` varchar(100) NOT NULL,
  `fullname_cus` varchar(50) NOT NULL,
  `mobile_cus` bigint(15) NOT NULL,
  `email_cus` varchar(30) NOT NULL,
  `fax_cus` varchar(30) NOT NULL,
  `customer_currency_cus` varchar(3) NOT NULL,
  `shipping_address_cus` text NOT NULL,
  `idcds_cus` int(11) NOT NULL,
  `created_date_cus` datetime NOT NULL DEFAULT current_timestamp(),
  `flag_cus` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_cus`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_details_cus`
--

INSERT INTO `customer_details_cus` (`id_cus`, `company_or_customer_name_cus`, `fullname_cus`, `mobile_cus`, `email_cus`, `fax_cus`, `customer_currency_cus`, `shipping_address_cus`, `idcds_cus`, `created_date_cus`, `flag_cus`) VALUES
(1, 'test123', 'qweq', 567856, 'sjk@sjk.com', 'qw', 'USD', '<p>test</p>', 1, '2020-07-27 07:00:00', 1),
(2, 'onemove', 'shridhar134', 7897687678, 'sjk@sjk.com', 'dsds', '', '', 1, '2020-07-27 07:00:00', 1),
(3, 'test', 'test', 567856, 'sjk@sjk.com', 'qw', '', '', 3, '2020-08-01 20:54:30', 1),
(4, 'New Company Name', 'New contact Person', 1231212, 'new@contact.com', '123', '', '', 6, '2020-08-01 20:57:33', 1),
(5, 'onemove software solutions ', 'shridhar kurani', 7878878787, 'sjkurani@gmail.com', 'test', '', '', 6, '2020-08-01 21:00:07', 1),
(6, 'Test save', 'Test save', 123, 'sjk@sjk.com', '1234', '', '', 6, '2020-08-01 21:05:17', 1),
(7, 'Save and next', 'Save and next', 123, 'sjk@sjk.com', '123s', '', '', 6, '2020-08-01 21:05:47', 1),
(8, 'New Customer', 'New pErson', 5555555, 'sjk1@sjk.com', '66666', '', '', 6, '2020-08-01 21:34:03', 1),
(9, 'New Customer1', 'New pErson', 55555551, 'sjk1@sjk.com', '66666', '', '', 6, '2020-08-01 21:34:57', 1),
(10, 'New Customer1', 'New pErson', 55555551, 'sjk1@sjk.com', '66666', '', '', 6, '2020-08-01 21:35:19', 1),
(11, 'eeee', 'eeee', 123, 'sjk@sjk.com', '23', '', '', 6, '2020-08-01 21:35:43', 1),
(12, 'eeeee', 'eeee', 123, 'sjk@sjk.com', '23', '', '', 6, '2020-08-01 21:36:37', 1),
(13, '1weweweewe', '1weweweewe', 123, 'sjk@sjk.com', '3sds', '', '', 6, '2020-08-01 21:38:10', 1),
(14, 'EEPRO SOFTWARES', 'shridhar', 7899452456, 'sjkurani@gmail.com', 'FAX2', '', '<p>#0001,&nbsp;</p>\r\n<p>2nd cross&nbsp;, 3rd building&nbsp;and&nbsp;test&nbsp;address</p>\r\n<p>Bangalore&nbsp;- 560100</p>', 7, '2020-08-01 23:50:14', 1),
(15, 'New customer with long name here New customer with long name here ', 'New customer', 1212312312, 'sjk@sjk.com', '123232', 'USD', '<p>Testing&nbsp;shipping&nbsp;address.&nbsp;Testing&nbsp;shipping&nbsp;address.&nbsp;Testing&nbsp;shipping&nbsp;address.&nbsp;Testing&nbsp;shipping&nbsp;address.&nbsp;</p>', 1, '2020-08-28 14:29:48', 1),
(16, 'rrr', 'rrr', 123, 'rr@rrc.com', '', 'INR', '', 1, '2020-08-29 12:05:21', 1),
(17, 'shridhar', 'shridhar kurani', 123, 'sjk@qs.com', '', 'INR', '', 1, '2020-08-29 12:05:47', 1),
(18, 'shridhar', 'shridhar kurani', 123, 'sjk@qs.com', '', 'INR', '', 1, '2020-08-29 12:06:01', 1),
(19, 'shridhar', 'shridhar kurani', 123, 'sjk@qs.com', '', 'INR', '<p>test</p>', 1, '2020-08-29 12:06:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_inv`
--

DROP TABLE IF EXISTS `invoice_inv`;
CREATE TABLE IF NOT EXISTS `invoice_inv` (
  `id_inv` int(11) NOT NULL AUTO_INCREMENT,
  `created_date_inv` datetime NOT NULL,
  `filesize_inv` int(11) NOT NULL,
  `idcus_inv` int(11) NOT NULL,
  PRIMARY KEY (`id_inv`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products_pts`
--

DROP TABLE IF EXISTS `products_pts`;
CREATE TABLE IF NOT EXISTS `products_pts` (
  `id_pts` int(11) NOT NULL AUTO_INCREMENT,
  `idcds_pts` int(11) NOT NULL,
  `name_pts` varchar(100) DEFAULT '',
  `file_name_pts` varchar(50) NOT NULL,
  `file_type_pts` varchar(20) NOT NULL,
  `orig_name_pts` varchar(100) NOT NULL,
  `file_size_pts` varchar(10) NOT NULL,
  `image_type_pts` varchar(10) NOT NULL,
  `image_size_str_pts` varchar(30) NOT NULL,
  `created_time_pts` datetime NOT NULL DEFAULT current_timestamp(),
  `is_image_pts` tinyint(1) NOT NULL,
  `flag_pts` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_pts`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_pts`
--

INSERT INTO `products_pts` (`id_pts`, `idcds_pts`, `name_pts`, `file_name_pts`, `file_type_pts`, `orig_name_pts`, `file_size_pts`, `image_type_pts`, `image_size_str_pts`, `created_time_pts`, `is_image_pts`, `flag_pts`) VALUES
(1, 0, '', '796084b42c07e12797f517135c158dc0.PNG', 'image/png', 'nginix-error.PNG', '32.74', 'png', 'width=\"1362\" height=\"550\"', '2020-11-08 01:27:59', 1, 1),
(2, 0, '', '4e2c650f5001915b8026116d2000853e.PNG', 'image/png', 'serviceURL.PNG', '81.23', 'png', 'width=\"1364\" height=\"650\"', '2020-11-08 01:27:59', 1, 1),
(3, 0, '', '880ca8e201955fb2f7ac432da69ddac7.PNG', 'image/png', 'nginix-error.PNG', '32.74', 'png', 'width=\"1362\" height=\"550\"', '2020-11-08 01:29:42', 1, 1),
(4, 0, '', '18dc22860303db6767151bf241cc5a76.PNG', 'image/png', 'serviceURL.PNG', '81.23', 'png', 'width=\"1364\" height=\"650\"', '2020-11-08 01:29:42', 1, 1),
(5, 0, '', '415778ed725e313d53651785ce703d90.png', 'image/png', 'Immagine1.png', '56.83', 'png', 'width=\"3840\" height=\"1080\"', '2020-11-08 01:36:13', 1, 1),
(6, 0, '', 'ff4c4ce03d221daee166d5ce337aec47.png', 'image/png', 'Immagine2.png', '33.27', 'png', 'width=\"228\" height=\"722\"', '2020-11-08 01:36:13', 1, 1),
(7, 0, '', '2e13f85f267f812abecafa895b081c4a.png', 'image/png', 'Firefox_Screenshot_2020-02-17T15-08-34.342Z_.png', '427.47', 'png', 'width=\"1349\" height=\"632\"', '2020-11-08 01:36:13', 1, 1),
(8, 0, '', 'Translations for MP-8716 (1).xlsx', 'application/vnd.open', '', '17974', '', '', '2020-11-08 01:36:13', 0, 1),
(9, 0, '', 'image.jpg', 'inode/x-empty', '', '0', '', '', '2020-11-08 01:37:54', 0, 1),
(10, 0, '', 'images (1).jpg', 'text/plain', '', '21', '', '', '2020-11-08 01:38:24', 0, 1),
(11, 0, '', '', '', '', '', '', '', '2020-11-08 01:52:35', 0, 1),
(12, 0, '', 'doc25216036961841.pdf', 'application/pdf', '', '109940', '', '', '2020-11-08 02:14:40', 0, 1),
(13, 0, '', '826ff71b398b2295fada1eb7a8cdd7ea.png', 'image/png', 'image-resizing-api.png', '62.35', 'png', 'width=\"582\" height=\"258\"', '2020-11-08 02:15:20', 1, 1),
(14, 0, '', 'b40e6884a58d117653297d9f2f0c68c6.PNG', 'image/png', 'nginix-error.PNG', '32.74', 'png', 'width=\"1362\" height=\"550\"', '2020-11-08 03:01:11', 1, 1),
(15, 0, '', 'ab9b9cfa852f90ad8a9a37f6dbca36a8.PNG', 'image/png', 'nginix-error.PNG', '32.74', 'png', 'width=\"1362\" height=\"550\"', '2020-11-08 03:06:59', 1, 1),
(16, 0, '', '0a786339912335295cdb539cf7af1ab5.PNG', 'image/png', 'Capture.PNG', '12.94', 'png', 'width=\"704\" height=\"152\"', '2020-11-08 03:12:46', 1, 1),
(17, 0, '', 'c779d48577f0ddd4f10c0263e196d701.PNG', 'image/png', 'Capture123.PNG', '125.98', 'png', 'width=\"1366\" height=\"768\"', '2020-11-08 03:12:46', 1, 1),
(18, 0, '', '75950a93aec5fb051ae66bb2a1dbeb4a.PNG', 'image/png', 'nginix-error.PNG', '32.74', 'png', 'width=\"1362\" height=\"550\"', '2020-11-08 03:12:46', 1, 1),
(19, 1, '', '9f9370d7b30d1b61738d66385283baeb.PNG', 'image/png', 'serviceURL.PNG', '81.23', 'png', 'width=\"1364\" height=\"650\"', '2020-11-08 03:12:46', 1, 1),
(20, 1, NULL, 'b2a311f4f2c783f14f38839166a6ef5d.PNG', 'image/png', 'Capture.PNG', '12.94', 'png', 'width=\"704\" height=\"152\"', '2020-11-08 12:57:28', 1, 1),
(21, 1, NULL, 'e60ea1d35041fd92ad17d01918a418ff.PNG', 'image/png', 'Capture123.PNG', '125.98', 'png', 'width=\"1366\" height=\"768\"', '2020-11-08 12:57:28', 1, 1),
(22, 1, NULL, '7d8807b71d2320af331726933d454dbe.PNG', 'image/png', 'nginix-error.PNG', '32.74', 'png', 'width=\"1362\" height=\"550\"', '2020-11-08 12:57:28', 1, 1),
(23, 1, NULL, '3be9d1c9a4e3d8b9933ee7978a72ddac.PNG', 'image/png', 'serviceURL.PNG', '81.23', 'png', 'width=\"1364\" height=\"650\"', '2020-11-08 12:57:28', 1, 1),
(24, 1, NULL, '34ad8a345798235cfff5140f23c1345c.PNG', 'image/png', 'Capture.PNG', '12.94', 'png', 'width=\"704\" height=\"152\"', '2020-11-08 12:57:40', 1, 1),
(25, 1, NULL, '0e79e43eafd1db9a2f4bb2398586db8b.PNG', 'image/png', 'Capture123.PNG', '125.98', 'png', 'width=\"1366\" height=\"768\"', '2020-11-08 12:57:40', 1, 1),
(26, 1, NULL, '71b62db94668f1d93970ddfede04617b.PNG', 'image/png', 'nginix-error.PNG', '32.74', 'png', 'width=\"1362\" height=\"550\"', '2020-11-08 12:57:40', 1, 1),
(27, 1, NULL, 'c03a26c1b6cbe529b49d147aaff2daa6.PNG', 'image/png', 'serviceURL.PNG', '81.23', 'png', 'width=\"1364\" height=\"650\"', '2020-11-08 12:57:40', 1, 1),
(28, 1, '', '1dc4ab3bfb000ddd25d2f2bfe430e352.PNG', 'image/png', 'Capture123.PNG', '125.98', 'png', 'width=\"1366\" height=\"768\"', '2020-11-08 12:58:32', 1, 1),
(29, 1, '', 'e2e86eab4c4b30dfc69840ab6289a4ee.PNG', 'image/png', 'Capture123.PNG', '125.98', 'png', 'width=\"1366\" height=\"768\"', '2020-11-11 23:30:04', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_order_details_pod`
--

DROP TABLE IF EXISTS `product_order_details_pod`;
CREATE TABLE IF NOT EXISTS `product_order_details_pod` (
  `id_pds` int(11) NOT NULL AUTO_INCREMENT,
  `produc_id_pds` int(11) NOT NULL,
  `product_detail_pds` text NOT NULL,
  `price_pds` int(11) NOT NULL,
  `quantity_pds` int(11) NOT NULL,
  PRIMARY KEY (`id_pds`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotations_qts`
--

DROP TABLE IF EXISTS `quotations_qts`;
CREATE TABLE IF NOT EXISTS `quotations_qts` (
  `id_qts` int(11) NOT NULL AUTO_INCREMENT,
  `fullurl_qts` varchar(1000) NOT NULL,
  `idcds_qts` int(11) NOT NULL,
  `datetime_qts` datetime NOT NULL DEFAULT current_timestamp(),
  `flag_qts` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_qts`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotations_qts`
--

INSERT INTO `quotations_qts` (`id_qts`, `fullurl_qts`, `idcds_qts`, `datetime_qts`, `flag_qts`) VALUES
(1, 'ds', 1, '2020-08-01 17:35:36', 1),
(2, 'dsdsds', 1, '2020-08-01 17:35:48', 1),
(3, 'ds', 12, '2020-08-01 17:39:29', 1),
(4, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"123\"},{\"Id\":\"product465\",\"qty\":\"2\"}]', 12, '2020-08-01 12:11:24', 1),
(5, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"123\"},{\"Id\":\"product465\",\"qty\":\"2\"}]', 12, '2020-08-01 12:12:13', 1),
(6, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"123\"},{\"Id\":\"product465\",\"qty\":\"2\"}]', 12, '2020-08-01 12:12:48', 1),
(7, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"123\"},{\"Id\":\"product465\",\"qty\":\"2\"}]', 12, '2020-08-01 12:12:54', 1),
(8, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"123\"},{\"Id\":\"product465\",\"qty\":\"2\"}]', 12, '2020-08-01 12:13:04', 1),
(9, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"123\"},{\"Id\":\"product465\",\"qty\":\"2\"}]', 12, '2020-08-01 12:13:36', 1),
(10, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"123\"},{\"Id\":\"product465\",\"qty\":\"2\"}]', 12, '2020-08-01 12:14:08', 1),
(11, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:11:23', 1),
(12, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:12:54', 1),
(13, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:13:00', 1),
(14, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:13:10', 1),
(15, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:13:18', 1),
(16, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:13:20', 1),
(17, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:14:05', 1),
(18, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:14:46', 1),
(19, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:15:53', 1),
(20, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:16:20', 1),
(21, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:16:37', 1),
(22, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:17:30', 1),
(23, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:17:48', 1),
(24, 'http://localhost/mbasket/quotations/preview/13?products=[{\"Id\":\"product459\",\"qty\":\"1\"}]', 6, '2020-08-01 16:19:24', 1),
(25, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"2\",\"p\":\"1\"}]', 1, '2020-08-09 09:45:59', 1),
(26, 'http://localhost/mbasket/quotations/preview/2?products=[{\"Id\":\"product459\",\"qty\":\"2\",\"p\":\"1\"}]', 1, '2020-08-09 09:46:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `session_table`
--

DROP TABLE IF EXISTS `session_table`;
CREATE TABLE IF NOT EXISTS `session_table` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_table`
--

INSERT INTO `session_table` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('8d29460c31e12fb3d3e7fb47b894441c', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36', 1612548702, 'a:1:{s:6:\"userDb\";s:14:\"kannada_second\";}');

-- --------------------------------------------------------

--
-- Table structure for table `user_usr`
--

DROP TABLE IF EXISTS `user_usr`;
CREATE TABLE IF NOT EXISTS `user_usr` (
  `id_usr` int(11) NOT NULL AUTO_INCREMENT,
  `displayname_usr` varchar(100) NOT NULL,
  `fullname_usr` varchar(50) NOT NULL,
  `mobile_usr` varchar(15) NOT NULL,
  `email_usr` varchar(30) NOT NULL,
  `nickname_usr` varchar(250) NOT NULL,
  `role_usr` varchar(11) NOT NULL,
  `databasename_usr` varchar(30) NOT NULL,
  `created_ip_usr` varchar(255) NOT NULL,
  `flag_usr` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0-Pending,1-active,2-blocked,3-deleted',
  PRIMARY KEY (`id_usr`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_usr`
--

INSERT INTO `user_usr` (`id_usr`, `displayname_usr`, `fullname_usr`, `mobile_usr`, `email_usr`, `nickname_usr`, `role_usr`, `databasename_usr`, `created_ip_usr`, `flag_usr`) VALUES
(1, 'shridhar kurani', 'shridhar kurani', '7899452456', 'sjkurani@gmail.com', 'sjkurani@gmail.com', 'admin', 'kannada_second', 'test', 1),
(3, 'EEPRO Admin', 'EEPRO Admin', '7899452456', 'admin@eepro.in', 'admin@eepro.in', 'admin', 'kannada_second', 'test', 1),
(4, 'EEPRO Admin', 'EEPRO Admin', '9341217091', 'info@shreeleaf.com', 'info@shreeleaf.com', 'customer', 'shreelea_wp685', 'test', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
