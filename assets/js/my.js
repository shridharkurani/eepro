$(document).ready(function() {
    var url=window.location.href;
    var arr=url.split('?')[1];

    window.onafterprint = function(){
        // window.location.reload(true);
        window.location.href = base_url + 'products?clearProducts=1';
    }

    $("#confirm").click(function (e) {
        var companyID = $(this).attr('id-cmpny');
        var custID = $(this).attr('id-customer');
        alert("You have confirmed quotation.");
        $.ajax({
            method: "POST",
            url: base_url+"ajax/confitmQuotation",
            dataType : "json",
            data: {
                url: url,
                companyID : companyID,
            }
        })
        .done(function( data ) {
            parsed_json_array = (data);
            // console.log(parsed_json_array);
            if(parsed_json_array) {
                // console.log(parsed_json_array)
            }
            /*if(parsed_json_array) {
                $('#link_post_subject').find('option').remove().end();
                var str1 = "<option value=0>--Select--</option>";
                var str;
                $.each( parsed_json_array, function( key, value ) {
                    str += "<option value = "+value.post_sub_id+">"+value.post_subject+"</option>";
                });
                $("#link_post_subject").append(str1 + str);
            }*/
        });
    });
    $("#confirm1").click(function () {
        var companyID = $(this).attr('id-cmpny');
        var custID = $(this).attr('id-customer');
        /*$.ajax({
            method: "POST",
            url: base_url + "ajax/confirmQuotation?,",
            success: function (area_array) {
                console.log(area_array);
            }
        });*/
    });
    var productTable = $('#productDataTable').DataTable({

        ajax: base_url+"ajax/get_products/parent",
        columns: [
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    return '<input type="hidden" name="productIds[]"  value="'+data+'" > <label>'+row.created+'</label>';
                },
                className: "text-center"
            },
            {
                data: "productImg",
                "render": function ( data, type, row, meta ) {
                    return '<img src="'+data+'" class="img-responsive img-thumbnail" style="width: 100% !important;" ">';
                }
            },{
                data: "productDesc",
                "render": function ( data, type, row, meta ) {
                    console.log(row);
                    if(row.productName == 0 || row.productName == ''){
                        return '<input class="countFields productDesc" type="text" placeholder="Enter Description" name="productDesc[]" id="productDesc'+data+'" value="" style="border:1px solid black">';
                    }
                    else {
                        return '<input class="countFields productDesc" type="text" placeholder="Enter Description" name="productDesc[]" id="productDesc'+data+'" value="'+row.productDesc+'" style="border:1px solid black">';
                    }
                },
                className: "text-center"
            },
        ],
    });

    $('body').on("keyup",'.countFields', function(){
        console.log('keyed');
        console.log($(this).hasClass('productPrice'));
        if($(this).hasClass('productPrice')) {
            Id = $(this).attr('id');
            price = $(this).val();
            idNumber = Number(Id.replace("unitPrice",""));

            qty = isNaN($("#productQty"+ idNumber ).val()) ? 1 : $("#productQty"+ idNumber ).val();
            price = isNaN(price) ? 0 : price;
        }
        else if($(this).hasClass('productQty')) {
            Id = $(this).attr('id');
            qty = $(this).val();
            idNumber = Number(Id.replace("productQty",""));

            price = isNaN($("#unitPrice"+ idNumber ).val()) ? 1 : $("#unitPrice"+ idNumber ).val();
            qty = isNaN(qty) ? 0 : qty;
        }
        netAmount = price * qty;
        $("#productPrice"+idNumber).text(netAmount);
    });

    $(".countFields").keyup(function () {
        console.log($(this));
    });

    $('#customerDataTable').DataTable({

        ajax: base_url+"ajax/get_all_customers",
        columns: [
            { data: "company_or_customer_name_cus" },
            { data: "fullname_cus" },
            { data: "email_cus" },
            { data: "mobile_cus" },
            { data: "fax_cus" },
            {
                data: "id_cus",
                "render": function ( data, type, row, meta ) {
                    return '<a href="'+base_url+'customers/adding/'+data+'">Edit Customer</a>';
                }
            },
            {
                data: "id_cus",
                "render": function ( data, type, row, meta ) {
                    // return "<input type='submit' value='Choose this customer' name='listingSubmit'/>";
                    return '<a class="btn btn-primary chooseCustomerBtn" data-toggle="modal" data-target="#expencesModal" data-id="'+data+'">choose this customer</a>';
                }
            },
        ],
    });
    $(document).on('click','.chooseCustomerBtn',function () {
        var customerID = $(this).attr('data-id');
       $("#expencesModal #customerId").val(customerID)
    });
    $("#productValidationForm").submit(function(e){
        var errorNum = 0;
        var checkboxCnt = 0;
        var productsArr = [];
        var errorArr = [];
        var tempProductQty = 0;
        $("input[name='isProductSelected']").each(function () {
            if($(this).prop("checked") == true) {
                checkboxCnt = checkboxCnt + 1;
                productId = $(this).attr('id');
                IdNumber = productId.replace('product','');
                productQty = $("#productQty"+IdNumber).val();
                unitPrice = $("#unitPrice"+IdNumber).val();
                if($.isNumeric(productQty) && $.isNumeric(unitPrice)) {
                    var product = {};
                    product['Id'] = productId;
                    product['qty'] = productQty;
                    product['p'] = unitPrice;
                    productsArr.push(product);
                }
                else {
                    errorNum = 2;
                }
            }
        });
        $("input[name='productQty[]']").each(function () {
            productQty = $(this).val();
            if(productQty.length !== 0) {
                tempProductQty = tempProductQty + 1;
            }
        });
        if(checkboxCnt == 0 && productsArr.length == 0) {
            errorNum = 1;
        }
        else if(tempProductQty != checkboxCnt && errorNum != 1) {
            if (tempProductQty < checkboxCnt)
                errorNum = 4;
            else {
                errorNum = 3;
            }
        }
        errorArr[1] = 'Select at least one product';
        errorArr[2] = 'Quantity should not be non numeric value. Please enter valid quantity number.';
        errorArr[3] = 'You have entered quantity but missed to choose the product.';
        errorArr[4] = 'Please enter valid quantity for chosen product.';
        if(errorNum > 0) {
            alert(errorArr[errorNum]);
            e.preventDefault();
        }
    });

  //Date format
  var date = new Date();
  date.setDate(date.getDate());

  $(".date_fields")
  .datetimepicker({
    format: "dd/mm/yyyy - HH:ii P",
    showMeridian: true,
    autoclose: true,
    todayBtn: "linked",
    startDate: date,
    linkField: "selected_pickup_date",
    linkFormat: "yyyy-mm-dd hh:ii"
  })
  .on('changeDate', function(ev){
    //assign_enddate();
  });

  var cnt = 0;
    $(window).scroll(function(){
      //alert($(this).scrollTop());
      if($("#transparent_back_img").length) {
        var targetOffset  = $("#transparent_back_img").offset().top - 400;
        if($(this).scrollTop() >targetOffset  && cnt == 0)
        {
          cnt = 1;
          $('.count').each(function () {
                  $(this).prop('Counter',0).animate({
                      Counter: $(this).text()
                  }, {
                      duration: 2500,
                      easing: 'swing',
                      step: function (now) {
                          $(this).text(Math.ceil(now));
                      }
                  });
              });

        }
      }
    });
  nanospell_url = base_url+'assets/nanospell/plugin.js';
	tinymce.init({
            selector: "textarea",
            plugins: "link image table searchreplace wordcount visualblocks insertdatetime media nonbreaking save",
            external_plugins: {"nanospell": nanospell_url},
            nanospell_server: "php"
   });


  });

$(document).ready(function() {
    var table = $('#example').DataTable({
        /*columnDefs: [{
            orderable: false,
            targets: [1,2,3]
        }],*/
        ajax: base_url+"ajax/get_products",
        columns: [
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.productIsChecked == 0){
                        return '<input type="hidden" name="productIds[]"  value="'+data+'" > <input type="checkbox" name="isProductSelected" id="product'+data+'">';
                    }
                    else {
                        return '<input type="hidden" name="productIds[]" value="'+data+'" > <input type="checkbox" checked="checked" name="isProductSelected" id="product'+data+'">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productImg",
                "render": function ( data, type, row, meta ) {
                    return '<img src="'+data+'" class="img-responsive img-thumbnail" style="width: 100% !important;" ">';
                },
                className: "text-center"
            },
            { data: "productName" },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.productDesc == 0 || row.productDesc == ''){
                        return '<input class="countFields productDesc" type="text" placeholder="Enter Description" name="productDesc[]" id="productDesc'+data+'" value="" style="border:1px solid black">';
                    }
                    else {
                        return '<input class="countFields productDesc" type="text" placeholder="Enter Description" name="productDesc[]" id="productDesc'+data+'" value="'+row.productDesc+'" style="border:1px solid black">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.productPrice == 0){
                        return '<input class="countFields productPrice" type="text" placeholder="Enter Price" name="productUnitPrice[]" id="unitPrice'+row.productId+'" value="" style="border:1px solid black">';
                    }
                    else {
                        return '<input class="countFields productPrice" type="text" placeholder="Enter Price" name="productUnitPrice[]" id="unitPrice'+row.productId+'" value="'+row.productPrice+'" style="border:1px solid black">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    console.log(row);
                    if(row.quantity == 0) {
                        return '<input class="countFields productQty" type="text" placeholder="Enter quantity" name="productQty[]" id="productQty'+data+'" value="" style="border:1px solid black">';
                    }
                    else {
                        return '<input class="countFields productQty" type="text" placeholder="Enter quantity" name="productQty[]" id="productQty'+data+'" value="'+row.quantity+'" style="border:1px solid black">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.amount == 0) {
                        return '<label name="productPrice'+data+'"  id="productPrice'+data+'" class="text-center"> 0 </label>';
                    }
                    else {
                        return '<label name="productPrice'+data+'"  id="productPrice'+data+'" class="text-center"> '+row.amount +'</label>';
                    }
                },
                className: "text-center"
            },
        ],
    });

    $('.productValidation').click( function() {
        var data = table.$('input').serialize();
        $("#fullData").val(data);
    } );

    $("#clearProductData").click(function (e) {
        $.ajax({
            method: "POST",
            url: base_url+"ajax/clearProductSessionData",
            dataType : "json",
            data: {
                /*url: url,
                companyID : companyID,*/
            }
        });
        e.preventDefault();
        location.href = base_url+'products';
    });
});

$(document).ready(function() {
    var table12 = $('#my_example').DataTable({
        /*columnDefs: [{
            orderable: false,
            targets: [1,2,3]
        }],*/
        ajax: base_url+"ajax/get_products",
        columns: [
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.productIsChecked == 0){
                        return '<input type="hidden" name="productIds[]"  value="'+data+'" > <input type="checkbox" name="isProductSelected" id="product'+data+'">';
                    }
                    else {
                        return '<input type="hidden" name="productIds[]" value="'+data+'" > <input type="checkbox" checked="checked" name="isProductSelected" id="product'+data+'">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productImg",
                "render": function ( data, type, row, meta ) {
                    return '<img src="'+data+'" class="img-responsive img-thumbnail" style="width: 100% !important;" ">';
                },
                className: "text-center"
            },
            { data: "productName" },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.productDesc == 0 || row.productDesc == ''){
                        return '<input class="countFields productDesc" type="text" placeholder="Enter Description" name="productDesc[]" id="productDesc'+data+'" value="" style="border:1px solid black">';
                    }
                    else {
                        return '<input class="countFields productDesc" type="text" placeholder="Enter Description" name="productDesc[]" id="productDesc'+data+'" value="'+row.productDesc+'" style="border:1px solid black">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.productPrice == 0){
                        return '<input class="countFields productPrice" type="text" placeholder="Enter Price" name="productUnitPrice[]" id="unitPrice'+row.productId+'" value="" style="border:1px solid black">';
                    }
                    else {
                        return '<input class="countFields productPrice" type="text" placeholder="Enter Price" name="productUnitPrice[]" id="unitPrice'+row.productId+'" value="'+row.productPrice+'" style="border:1px solid black">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    console.log(row);
                    if(row.quantity == 0) {
                        return '<input class="countFields productQty" type="text" placeholder="Enter quantity" name="productQty[]" id="productQty'+data+'" value="" style="border:1px solid black">';
                    }
                    else {
                        return '<input class="countFields productQty" type="text" placeholder="Enter quantity" name="productQty[]" id="productQty'+data+'" value="'+row.quantity+'" style="border:1px solid black">';
                    }
                },
                className: "text-center"
            },
            {
                data: "productId",
                "render": function ( data, type, row, meta ) {
                    if(row.amount == 0) {
                        return '<label name="productPrice'+data+'"  id="productPrice'+data+'" class="text-center"> 0 </label>';
                    }
                    else {
                        return '<label name="productPrice'+data+'"  id="productPrice'+data+'" class="text-center"> '+row.amount +'</label>';
                    }
                },
                className: "text-center"
            },
        ],
    });
});